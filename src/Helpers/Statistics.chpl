/*
	This module contains functions for basic statistics.
*/
module Statistics {
	/*
		Returns the efficiency (average speedup per core).\\

		===== Parameters:
		* timeSequential: real
			** the elapsed sequential time
		* timeParallel: real
			** the elapsed parallel time
		* numCores: int
			** the number of cores
		===== Returns:
		* real
	*/
	proc efficiency(timeSequential: real, timeParallel: real, numCores: int) {
		return speedup(timeSequential, timeParallel) / numCores;
	}

	/*
		Returns the speedup of a parallel computation time compared to a
		sequential one.\\

		===== Parameters:
		* timeSequential: real
			** the elapsed sequential time
		* timeParallel: real
			** the elapsed parallel time
		===== Returns:
		* real
	*/
	proc speedup(timeSequential: real, timeParallel: real) {
		return timeSequential / timeParallel;
	}
}
