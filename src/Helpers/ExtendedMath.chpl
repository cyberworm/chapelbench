/*
	This module contains mathematical functions that are not part of the
	official Chapel distribution.
*/
module ExtendedMath {

	/*
		Factorizes a number into factors of 2 and a remainding odd value.
		Returns a tuple containing the amount of 2-factors and the remainder.\\

		===== Parameters:
		* value: uint
			** the number to be factorized
		===== Returns:
		* (uint, uint)
	*/
	proc factorizeByTwos(value: uint) {
		var residue: uint = value;
		var numTwos: uint = 0;
		var remaining: uint;
		while(residue % 2 == 0) {
			numTwos += 1;
			residue /= 2;
		}

		remaining = residue;
		return (numTwos, remaining);
	}

	/*
		Does a right-to-left binary modular exponentation. Useful for large
		exponents as it prevents overflows. Returns the remainder.\\

		===== Parameters:
		* base: uint
			** the base of the exponentation
		* power: uint
			** the exponent
		* mod: uint
			** the modulus
		===== Returns:
		* uint
	*/
	proc modExp(base: uint, power: uint, mod: uint) {
		// http://en.wikipedia.org/wiki/Modular_exponentiation
		var result: uint = 1;
		var exponent: uint = power;
		var newBase: uint = base;
		while (exponent > 0) {
			if (exponent % 2 == 1) {
				result = modMul(result, newBase, mod);
			}
			exponent = (exponent >> 1);
			newBase = modMul(newBase, newBase, mod);
		}
		return result;
	}

	/*
		Does a modular multiplication. Useful for large factors as it prevents
		overflows. Based on the "Russian Peasant Multiplication" algorithm.
		Returns the remainder.\\

		===== Parameters:
		* first: uint
			** the first factor of the multiplication
		* second: uint
			** the second factor of the multiplication
		* mod: uint
			** the modulus
		===== Returns:
		* uint
	*/
	proc modMul(first: uint, second: uint, mod: uint) {
		// http://stackoverflow.com/questions/12168348/ways-to-do-modulo-multiplication-with-primitive-types
		// http://en.wikipedia.org/wiki/Ancient_Egyptian_multiplication#Russian_peasant_multiplication
		var firstFactor: uint = first;
		var secondFactor: uint = second;
		var result: uint = 0;
		while (firstFactor) {
			if (firstFactor & 1) {
				result = (result + secondFactor) % mod;
			}
			firstFactor = (firstFactor >> 1);
			secondFactor = (secondFactor << 1) % mod;
		}
		return result;
	}

	/*
		Checks whether a given number is prime or not. Uses the deterministic
		Miller-Rabin Primality test. Returns a boolean.\\

		===== Parameters:
		* value: uint
			** the number to check for primality
		===== Returns:
		* bool
	*/
	proc isPrime(value: uint) {
		// http://gandraxa.com/miller_rabin_primality_test.xml
		// http://www.bitnuts.de/miller_rabin.pdf
		if (value <= 1 || (value > 2 && value % 2 == 0)) {
			return false;
		}
		if (value == 2 || value == 7 || value == 61) {
			return true;
		}

		/*
		since the square root of max(uint(64) is below the threshhold of
		4,759,123,141, we only need to check against the wittnesses of 2, 7 and
		61.
		*/
		var witnesses: [1..3] int = [2, 7, 61];
		var m: uint = value - 1;
		// display m as (2^s)*d
		var mFactorization: (uint, uint) = factorizeByTwos(m);
		var s: uint = mFactorization(1);
		var d: uint = mFactorization(2);

		// this will hold the remainders
		var list: [0..s] uint;


		for a in witnesses {
			// a_0 = (a**d) % n
			list[0] = modExp(a:uint, d, value);
				for r in 1..s {
					// a_r = (a**((2**r)*d)) % n
					list[r] = modExp(a:uint, ((2**r)*d), value);
					if (list[r] == 1 && list[r-1] != 1 && list[r-1] != m) {
						return false;
					}
				}
				if (list[s] != 1) {
					return false;
				}
		}
		return true;
	}
}
