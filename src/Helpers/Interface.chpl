/*
	This module contains functions used for the user interface. It also includes
	the ncurses wrapper module.
*/
module Interface {
	use Ncurses;
	use Formatting;

	/*
		Asks for user input as how to continue after a finished test. Returns
		a boolean to determine whether the process should be ended or not.\\

		===== Parameters:
		* fileName: string
			** the name of the log file to display for the user's convenience
		* bufferedData: list(string)
			** the name of the log file to display for the user's convenience

		==== Returns:
		* bool
	*/
	proc postTestQuery(fileName: string, ref bufferedData: list(string)) {
		nc_noecho();
		nc_curs_set(0);
		nc_color_set(4);
		var scroller = new ScrollArea(bufferedData);

		nc_getmaxyx();
		nc_mvaddstrln(NC_Y-2, 0, "Done! Enter 'm' to return to the menu or 'q' to exit.\n");

		nc_color_set(1);
		var input: int;

		while (input != 109) { // "m" is pressed
			input = nc_getch();
			select (input) {
				when 113 { // "q" is pressed
					nc_endwin();
					return true;
				}
				when NC_KEY_UP {
					scroller.scrollUp();
				}
				when NC_KEY_DOWN {
					scroller.scrollDown();
				}
			}
		}
		return false;
	}

	/*
		Provides a scrollable area for use with ncurses. ScrollArea takes
		an array of string (for example a buffer) and lets the user scroll up
		and down so an area which size depends on what has been set with
		nc_setscrreg().
	*/
	record ScrollArea {
		var dataDom: domain(1) = {1..1};
		var data: [dataDom] string;
		var dataPointerStart: int = 1;
		var dataPointerEnd: int = 1;

		/*
			Constructor that takes an array of strings. It initializes the
			domain of the ScrollArea's data and sets the view area according
			to what has been set with nc_setscrreg().\\

			===== Parameters:
			* data: [] string
				** array of strings to scroll over
		*/
		proc ScrollArea(data: [] string) {
			this.dataDom = data.domain;
			this.data = data;
			if (this.data.numElements > NC_SCROLL_LINES) {
				this.dataPointerStart = this.data.numElements - NC_SCROLL_LINES;
			}
			this.dataPointerEnd = this.data.numElements;
		}

		/*
			Constructor that takes a list of strings. It initializes the
			domain of the ScrollArea's data and sets the view area according
			to what has been set with nc_setscrreg().\\

			===== Parameters:
			* data: list(string)
				** list of strings to scroll over
		*/
		proc ScrollArea(ref data: list(string)) {
			this.dataDom = {1..(data.length+1)};
			var idx = 1;
			for item in data {
				this.data[idx] = item;
				idx += 1;
			}
			data.destroy();
			if (this.data.numElements > NC_SCROLL_LINES) {
				this.dataPointerStart = this.data.numElements - NC_SCROLL_LINES;
			}
			this.dataPointerEnd = this.data.numElements;
		}

		/*
			Returns the data size.\\

			===== Returns:
			* int
		*/
		proc getDataSize() {
			return this.data.numElements;
		}

		/*
			Returns the log buffer elements within a range. By default it returns
			all elements.\\

			===== Parameters:
			* pos: range
				** the range of elements to return. Defaults to all elements.
			===== Returns:
			* Array: string
		*/
		proc getData(pos: range = 1..data.numElements) {
			return this.data[pos];
		}

		/*
			Returns a log buffer element at a specific position.\\

			===== Parameters:
			* pos: int
				** the position of the element in the buffer
			===== Returns:
			* string
		*/
		proc getData(pos: int) {
			return this.data[pos];
		}

		/*
			Prints the visible data of the current view field.
		*/
		proc printViewField() {
			nc_clear();
			nc_color_set(1);
			var idx: c_int = 0;
			for i in {this.dataPointerStart..this.dataPointerEnd} {
				nc_mvaddstr(idx, 0, this.data[i]);
				idx += 1;
			}
		}

		/*
			Scrolls the data down and increments the markers of the viewable
			area.
		*/
		proc scrollDown() {
			if (this.dataPointerEnd != this.data.numElements) {
				nc_scrl(1);
				this.dataPointerStart += 1;
				this.dataPointerEnd += 1;
				nc_mvaddstr(NC_SCROLL_LINES, 0, this.data[this.dataPointerEnd]);
			}
		}

		/*
			Scrolls the data up and decrements the markers of the viewable
			area.
		*/
		proc scrollUp() {
			if (this.dataPointerStart != 1) {
				nc_scrl(-1);
				this.dataPointerStart -= 1;
				this.dataPointerEnd -= 1;
				nc_mvaddstr(0, 0, this.data[this.dataPointerStart]);
			}
		}

		/*
			Changes the current data.\\

			===== Parameters:
			* data: [] string
				** array of strings containing the new data
		*/
		proc setData(data: [] string) {
			this.data = data;
		}

		/*
			Changes the current data.\\

			===== Parameters:
			* data: list(string)
				** list of strings containing the new data
		*/
		proc setData(ref data: list(string)) {
			var idx = 1;
			for item in data {
				this.data[idx] = item;
				idx += 1;
			}
			data.destroy();
		}
	}

	/*
		Asks the user for a name for the log file and returns it. In case of
		no input the default log file name will be applied.\\

		===== Parameters:
		* substring: string
			** the substring that is included in the default log file name
		===== Returns:
		* string
	*/
	proc setLogFileName(substring: string) {
		var fileName: string = "";
		nc_echo();
		nc_color_set(3);
		nc_move(NC_SCROLL_LINES+2, 0);
		nc_clrtoeol();
		nc_addstr("Enter new log file name:");
		nc_color_set(1);
		nc_move(NC_Y, NC_X+1);
		if (nc_getstr(fileName) == 0 && fileName == "") {
			// fileName += ".log";
		// } else {
			fileName = "ChapelBench_" + substring + "_" +
					   Formatting.getFormattedDate("") +
					   "_" + Formatting.getFormattedTime("");
		}
		return fileName;
	}

	/*
		Asks the user for a positive value to set the number of test runs and
		returns it.\\

		===== Returns:
		* uint
	*/
	proc setRuns() {
		var numRuns: uint;
		nc_echo();
		nc_color_set(3);
		nc_move(NC_SCROLL_LINES+2, 0);
		nc_clrtoeol();
		nc_addstr("Set number of test runs:");
		nc_color_set(1);
		nc_move(NC_Y, NC_X+1);
		var success = nc_getuint();
		if (success) {
			numRuns = success;
		}
		if (numRuns == 0) {
			numRuns = 3;
		}
		return numRuns;
	}

	/*
		Asks the user for an unsigned value and returns it.\\

		===== Parameters:
		* displayedText: string
			** the text to display to inform the user of the required input
		===== Returns:
		* uint
	*/
	proc setUnsignedValue(displayedText: string) {
		var value: uint;
		nc_echo();
		nc_color_set(3);
		nc_move(NC_SCROLL_LINES+2, 0);
		nc_clrtoeol();
		nc_addstr(displayedText);
		nc_color_set(1);
		nc_move(NC_Y, NC_X+1);
		var success = nc_getuint();
		if success {
			value = success;
		}
		return value;
	}

	/*
		Asks the user for a signed value and returns it.\\

		===== Parameters:
		* displayedText: string
			** the text to display to inform the user of the required input
		===== Returns:
		* int
	*/
	proc setValue(displayedText: string) {
		var value: int;
		nc_echo();
		nc_color_set(3);
		nc_move(NC_SCROLL_LINES+2, 0);
		nc_clrtoeol();
		nc_addstr(displayedText);
		nc_color_set(1);
		nc_move(NC_Y, NC_X+1);
		var success = nc_getint();
		if success {
			value = success;
		}
		return value;
	}
}
