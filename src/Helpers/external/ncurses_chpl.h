/* Original variables and constants */
extern const int NC_KEY_DOWN;
extern const int NC_KEY_UP;
extern const short NC_COLOR_BLACK;
extern const short NC_COLOR_RED;
extern const short NC_COLOR_GREEN;
extern const short NC_COLOR_YELLOW;
extern const short NC_COLOR_BLUE;
extern const short NC_COLOR_MAGENTA;
extern const short NC_COLOR_CYAN;
extern const short NC_COLOR_WHITE;

/* Added variables and constants */
extern int NC_X, NC_Y, NC_SCROLL_LINES;

// Original functions
int nc_addstr(const char *str);

int nc_clear();

int nc_clrtoeol();

int nc_color_set(short color_pair_number);

int nc_curs_set(int visibility);

int nc_echo();

int nc_endwin();

int nc_getch();

void nc_getbegyx();

void nc_getmaxyx();

int nc_getstr(char* str);

void nc_getyx();

void nc_initscr(void);

int nc_init_pair(short pair, short f, short b);

int nc_keypad(int bf);

int nc_move(int y, int x);

int nc_mvaddstr(int y, int x, const char *str);

int nc_mvprintw(int y, int x, const char *fmt, ...);

int nc_noecho();

int nc_refresh();

int nc_scrl(int n);

int nc_scrollok(int bf);

int nc_setscrreg(int top, int bot);

int nc_set_tabsize(int size);

int nc_start_color(void);

int nc_use_default_colors(void);

// Added functions
int nc_addstrln(const char *str);

unsigned long long int nc_getuint();

long long int nc_getint();

int nc_mvaddstrln(int y, int x, const char *str);

const char* nc_raddstr(const char *str);

const char* nc_raddstrln(const char *str);

const char* nc_rmvaddstr(int y, int x, const char *str);

const char* nc_rmvaddstrln(int y, int x, const char *str);
