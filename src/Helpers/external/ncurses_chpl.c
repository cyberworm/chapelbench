#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include "ncurses_chpl.h"

/* Original variables and constants */
const int NC_KEY_DOWN = KEY_DOWN;
const int NC_KEY_UP = KEY_UP;
const short NC_COLOR_BLACK = COLOR_BLACK;
const short NC_COLOR_RED = COLOR_RED;
const short NC_COLOR_GREEN = COLOR_GREEN;
const short NC_COLOR_YELLOW = COLOR_YELLOW;
const short NC_COLOR_BLUE = COLOR_BLUE;
const short NC_COLOR_MAGENTA = COLOR_MAGENTA;
const short NC_COLOR_CYAN = COLOR_CYAN;
const short NC_COLOR_WHITE = COLOR_WHITE;

/* Added variables and constants */
int NC_X, NC_Y, NC_SCROLL_LINES;

/* Original functions */
int nc_addstr(const char *str) {
	int success = addstr(str);
	nc_getyx();
	nc_refresh();
	return success;
}

int nc_clear() {
	int success = clear();
	return success;
}

int nc_clrtoeol() {
	int success = clrtoeol();
	return success;
}

int nc_color_set(short color_pair_number) {
	int success = color_set(color_pair_number, 0);
	return success;
}

int nc_curs_set(int visibility) {
	int success = curs_set(visibility);
	return success;
}

int nc_echo() {
	int success = echo();
	return success;
}

int nc_endwin() {
	int success = endwin();
	return success;
}

void nc_getbegyx() {
	getbegyx(stdscr, NC_Y, NC_X);
}

int nc_getch() {
	int success = getch();
	return success;
}

void nc_getmaxyx() {
	getmaxyx(stdscr, NC_Y, NC_X);
}

int nc_getstr(char* str) {
	int success = getstr(str);
	return success;
}

void nc_getyx() {
	getyx(stdscr, NC_Y, NC_X);
}

void nc_initscr() {
	initscr();
}

int nc_init_pair(short pair, short f, short b) {
	int success = init_pair(pair, f, b);
	return success;
}

int nc_keypad(int bf) {
	int success = keypad(stdscr, bf);
	return success;
}

int nc_move(int y, int x) {
	int success = move(y, x);
	nc_getyx();
	nc_refresh();
	return success;
}

int nc_mvaddstr(int y, int x, const char *str) {
	int success = mvaddstr(y, x, str);
	nc_getyx();
	nc_refresh();
	return success;
}

int nc_mvprintw(int y, int x, const char *fmt, ...) {
	va_list argptr;

	int code, size = 100;
	char *str;
	/* code taken from 'man vsnprintf' example */
	if ((str = malloc (size)) == NULL) {
		return -1;
	}

	while (1) {
		/* Try to print in the allocated space. */
		va_start(argptr, fmt);
		code = vsnprintf(str, size, fmt, argptr);
		va_end(argptr);
		/* If that worked, return the string. */
		if (code > -1 && code < size) {
			int success = mvprintw(y, x, str);
			free(str);
			nc_getyx();
			nc_refresh();
			return success;
		}
		/* Else try again with more space. */
		if (code > -1) {    /* glibc 2.1 */
			size = code+1; /* precisely what is needed */
		} else {           /* glibc 2.0 */
			size *= 2;  /* twice the old size */
		}
		if ((str = realloc (str, size)) == NULL) {
			return -1;
		}
	}
}

int nc_noecho() {
	int success = noecho();
	return success;
}

int nc_refresh() {
	int success = refresh();
	return success;
}

int nc_scrl(int n) {
	int success = scrl(n);
	nc_refresh();
	return success;
}

int nc_scrollok(int bf) {
	int success = scrollok(stdscr, bf);
	return success;
}

int nc_setscrreg(int top, int bot) {
	NC_SCROLL_LINES = bot;
	int success = setscrreg(top, bot);
	return success;
}

int nc_set_tabsize(int size) {
	int success = set_tabsize(size);
	return success;
}

int nc_start_color(void) {
	int success = start_color();
	return success;
}

int nc_use_default_colors(void) {
	int success = use_default_colors();
	return success;
}

/* Added functions */
int nc_addstrln(const char *str) {
	int size = strlen(str) + 1;
	char string[size];
	strcpy(string, str);
	int success = addstr(strcat(string, "\n"));
	nc_getyx();
	nc_refresh();
	return success;
}

unsigned long long int nc_getuint() {
	char str[80];
	char *endptr;
	errno = 0;
	unsigned long long int result;
	int success = getstr(str);
	result = strtoull(str, &endptr, 10);
	if (success != 0 || ((result == 0 || result == ULLONG_MAX) && errno == ERANGE)) {
		return 0;
	}
	return result;
}

long long int nc_getint() {
	char str[80];
	char *endptr;
	errno = 0;
	int result;
	int success = getstr(str);
	result = strtoll(str, &endptr, 10);
	if (success != 0 || ((result == LLONG_MIN || result == LLONG_MAX) && errno == ERANGE)) {
		return 0;
	}
	return result;
}

int nc_mvaddstrln(int y, int x, const char *str) {
	int size = strlen(str) + 1;
	char string[size];
	strcpy(string, str);
	int success = mvaddstr(y, x, strcat(string, "\n"));
	nc_getyx();
	nc_refresh();
	return success;
}

const char* nc_raddstr(const char *str) {
	int success = addstr(str);
	nc_getyx();
	nc_refresh();
	return str;
}

const char* nc_raddstrln(const char *str) {
	int size = strlen(str) + 1;
	char string[size];
	strcpy(string, str);
	int success = addstr(strcat(string, "\n"));
	nc_getyx();
	nc_refresh();
	return str;
}

const char* nc_rmvaddstr(int y, int x, const char *str) {
	int success = mvaddstr(y, x, str);
	nc_getyx();
	nc_refresh();
	return str;
}

const char* nc_rmvaddstrln(int y, int x, const char *str) {
	int size = strlen(str) + 1;
	char string[size];
	strcpy(string, str);
	int success = mvaddstr(y, x, strcat(string, "\n"));
	nc_getyx();
	nc_refresh();
	return str;
}
