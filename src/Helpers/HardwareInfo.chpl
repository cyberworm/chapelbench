/*
	This module contains functions for retreiving information about the used
	hardware.
*/
module HardwareInfo {
	use Memory;

	/*
		Returns the CPU ID. Currently only one processor is supported.\\

		===== Returns:
		* int
	*/
	proc getCPUID() {
		return here.id;
	}

	/*
		Returns the host name of the computer with the installed CPU. Currently
		only one processor is supported.\\

		===== Returns:
		* string
	*/
	proc getCPUName() {
		return here.name;
	}

	/*
		Returns basic hardware information (CPU ID, number of cores, total
		RAM) about the computer as a string. Currently only one processor
		is supported.\\

		===== Returns:
		* string
	*/
	proc getHardwareInfo() {
		return ("CPU " + getCPUID() + ": " + getNumCores() + " cores, " + getMemory(MemUnits.MB) + " MB total memory");
	}

	/*
		Returns the total RAM. Currently only one processor is supported.\\

		===== Parameters:
		* unit: MemUnits
			** the desired unit (Bytes, KB, MB, GB). Defaults to Bytes.
		===== Returns:
		* int
	*/
	proc getMemory(unit: MemUnits=MemUnits.Bytes) {
		return here.physicalMemory(unit);
	}

	/*
		Returns the number of CPU cores. Currently only one processor is
		supported.\\

		===== Returns:
		* int
	*/
	proc getNumCores() {
		return here.numCores;
	}

	/*
		Returns the number of available CPUs.\\

		===== Returns:
		* int
	*/
	proc getNumCPUs() {
		return numLocales;
	}
}
