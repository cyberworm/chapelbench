/*
	ncurses wrapper module for Chapel. It prototypes custom C functions
	dedicated to creating an interface to the ncurses text user interface
	library. In general function names minus the nc_-prefix correspond to the
	actual ncurses functions, though some new functions have been added for
	ease of use. Also some original functions have been "extended" (mostly
	to automatically get the current curser position). The function
	definitions and declarations can be
	found in

	  * ./external/ncurses_chpl.c
	  * ./external/ncurses_chpl.h

	For further information about ncurses consult the documentation at
	http://invisible-island.net/ncurses/man/ncurses.3x.html .
*/
module Ncurses {
	use SysCTypes;

	// Original variables and constants
	extern const NC_COLOR_BLACK: c_short;
	extern const NC_COLOR_RED: c_short;
	extern const NC_COLOR_GREEN: c_short;
	extern const NC_COLOR_YELLOW: c_short;
	extern const NC_COLOR_BLUE: c_short;
	extern const NC_COLOR_MAGENTA: c_short;
	extern const NC_COLOR_CYAN: c_short;
	extern const NC_COLOR_WHITE: c_short;
	extern const NC_KEY_DOWN: c_int;
	extern const NC_KEY_UP: c_int;

	// Added variables and constants
	extern var NC_X: c_int;
	extern var NC_Y: c_int;
	extern var NC_SCROLL_LINES: c_int;

	// Original functions
	extern proc nc_addstr(str: string);
	extern proc nc_clear(): c_int;
	extern proc nc_clrtoeol(): c_int;
	extern proc nc_color_set(color_pair_number: c_short): c_int;
	extern proc nc_curs_set(visibility: c_int): c_int;
	extern proc nc_echo(): c_int;
	extern proc nc_endwin(): c_int;
	extern proc nc_getbegyx();
	extern proc nc_getch(): c_int;
	extern proc nc_getmaxyx();
	extern proc nc_getstr(str: string): c_int;
	extern proc nc_getyx();
	extern proc nc_initscr();
	extern proc nc_init_pair(pair: c_short, f: c_short, b: c_short);
	extern proc nc_keypad(bf: bool): c_int;
	extern proc nc_move(y: c_int, x: c_int);
	extern proc nc_mvaddstr(y: c_int, x: c_int, str: string): c_int;
	extern proc nc_mvprintw(y: c_int, x: c_int, fmt: string, vals...?numvals): c_int;
	extern proc nc_noecho(): c_int;
	extern proc nc_refresh(): c_int;
	extern proc nc_scrl(n: c_int): c_int;
	extern proc nc_scrollok(bf: bool): c_int;
	extern proc nc_setscrreg(top: c_int, bot: c_int): c_int;
	extern proc nc_set_tabsize(size: c_int): c_int;
	extern proc nc_start_color(): c_int;
	extern proc nc_use_default_colors(): c_int;

	// Added functions
	extern proc nc_addstrln(str: string);
	extern proc nc_getuint(): c_ulonglong;
	extern proc nc_getint(): c_longlong;
	extern proc nc_mvaddstrln(y: c_int, x: c_int, str: string): c_int;
	extern proc nc_raddstr(str: string): string;
	extern proc nc_raddstrln(str: string): string;
	extern proc nc_rmvaddstr(y: c_int, x: c_int, str: string): string;
	extern proc nc_rmvaddstrln(y: c_int, x: c_int, str: string): string;

	// Non-ncurses functions
	extern proc system(command: string): int;
}
