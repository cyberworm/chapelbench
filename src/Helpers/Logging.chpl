/*
	This module contains a basic logging framework.
*/
module Logging {
	use List;
	use Formatting;

	/*
		A very basic logging framework. It uses the standard Chapel file I/O
		and adds timestamps to each logged line. After each write the file is
		being synced to the file system to prevent data loss.
	*/
	record Logger {
		var logfile: file;
		var writer: channel(true, iokind.dynamic, true);
		var err: syserr;
		var logBuffer: list(string);

		/*
			The default constructor which opens a file in the OS's temporary
			directory (e.g. /tmp).\\
		*/
		proc Logger() {
			this.logfile = opentmp(iomode.rw, error=err);
			if (err == ENOENT) {
				this.logfile = opentmp(iomode.cwr);
			}
			this.writer = logfile.writer();
			this.logBuffer = new list(string);
		}

		/*
			Constructor for logging to an actual file with a specific
			path and/or file name.\\

			===== Parameters:
			* fileName: string
				** the path and/or name of the log file
		*/
		proc Logger(fileName: string) {
			this.logfile = open(fileName, iomode.rw, error=err);
			if (err == ENOENT) {
				this.logfile = open(fileName, iomode.cwr);
			}
			this.writer = logfile.writer();
			this.logBuffer = new list(string);
		}

		/*
			Writes data into the log file and into a buffer. Prepends a
			timestamp.\\

			===== Parameters:
			* data: string
				** the data to log
			* newline: bool
				** flag to determine whether a newline should be appended
			* timestmp: bool
				** flag to determine whether a timestamp should be prepended.\\
				   Only effective when newline is set to false.
			* buffered: bool
				** flag to determine whether the logged data should be buffered
		*/
		proc log(data: string, newline: bool = true, timestmp: bool = true, buffered: bool = true) {
			if (buffered) {
				this.logBuffer.append(data);
			}
			if (newline) {
				if (timestmp) {
					this.writer.writeln(timestamp() + data);
				} else  {
					this.writer.writeln(data);
				}
			} else {
				if (timestmp) {
					this.writer.write(timestamp() + data);
				} else {
					this.writer.write(data);
				}
			}
			this.writer.flush();
			this.logfile.fsync();
		}

		/*
			Closes the log file and returns the buffered data.\\

			===== Returns:
			* list(string)
		*/
		proc close() {
			this.writer.flush();
			this.writer.close();
			this.logfile.fsync();
			this.logfile.close();
			return this.logBuffer;
		}
	}
}
