/*
	This module contains functions for formatting, such as the current time or a
	large number.
*/
module Formatting {
	use Time;

	/*
		Returns the current date in the format YYYY-MM-DD as string.\\

		===== Parameters:
		* delimiter: string
			** the string to delimit year, month and day. Defaults to "-".
		===== Returns:
		* string
	*/
	proc getFormattedDate(delimiter: string = "-") {
		var date: (int, int, int) = getCurrentDate();
		var year: int = date[1];
		var month: int = date[2];
		var day: int = date[3];

		var formattedDate: string = format("%d", year) + delimiter + format("%02d", month) + delimiter + format("%02d", day);

		return formattedDate;
	}

	/*
		Returns the current time in the format HH:MM:SS as string.\\

		===== Parameters:
		* delimiter: string
			** the string to delimit hours, minutes and seconds. Defaults to ":".
		===== Returns:
		* string
	*/
	proc getFormattedTime(delimiter: string = ":") {
		var seconds: real = getCurrentTime(TimeUnits.seconds);
		var minutes: real = getCurrentTime(TimeUnits.minutes);
		var hours: real = getCurrentTime(TimeUnits.hours);

		// some calculation has as Chapel returns everything as real
		var actualHours: int = hours:int(32);
		var actualMinutes: int = (((minutes / 60) - hours:int(32)) * 60):int(32);
		var actualSeconds: int = (((seconds / 60) - minutes:int(32)) * 60):int(32);

		var formattedTime: string = format("%02d", actualHours) + delimiter + format("%02d", actualMinutes) + delimiter + format("%02d", actualSeconds);

		return formattedTime;
	}

	/*
		Returns a number as string with thousand seperators.\\

		===== Parameters:
		* number
			** the number to format
		* delimiter: string
			** the string to use as thousand seperator. Defaults to ",".
		===== Returns:
		* string
	*/
	proc getFormattedNumber(number, delimiter: string = ",") {
		var tmp: string = number:string;
		var formattedNumber: string;
		// var start: int = 1;
		var limit: int;

		// check if signed or unsigned
		if (tmp.substring(1) == "-") {
			// start = 2;
			tmp = tmp.substring(2..tmp.size);
			formattedNumber += "-";
		}

		// count digits before decimal mark
		for i in 1..tmp.size {
			if (tmp.substring(i) != ".") {
				limit += 1;
			} else {
				break;
			}
		}

		// format the number
		for i in 1..limit {
			formattedNumber += tmp.substring(i);
			if (i < limit) {
				if ((limit - i) % 3 == 0) {
					formattedNumber += delimiter;
				}
			}
		}

		// add digits after decimal mark
		if (limit != tmp.size) {
			for i in (limit + 1)..tmp.size {
				formattedNumber += tmp.substring(i);
			}
		}

		return formattedNumber;
	}

	/*
		Returns a timestamp in the format [YYYY-MM-DD HH:MM:SS] as string.\\

		===== Parameters:
		* dateDelimiter: string
			** the string to delimit year, month and day. Defaults to "-".
		* timeDelimiter: string
			** the string to delimit hours, minutes and seconds. Defaults to ":".
		===== Returns:
		* string
	*/
	proc timestamp(dateDelimiter: string = "-", timeDelimiter: string = ":") {
		return "[" + getFormattedDate(dateDelimiter) + " " + getFormattedTime(timeDelimiter) + "] ";
	}
}
