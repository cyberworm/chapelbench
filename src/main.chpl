/*
	This is the user interface and main entry point for the application.
*/
var VERSION = "v0.8";
use Benchmarks;
use Helpers.Interface;

/*
	Program entry point. Displays the main menu, handles user interaction and
	runs the tests
*/
proc main() {
	nc_initscr();
	nc_scrollok(true);
	nc_keypad(true);
	nc_use_default_colors();
	nc_start_color();
	nc_getmaxyx();
	nc_setscrreg(0, NC_Y-4);
	nc_set_tabsize(2);
	nc_init_pair(1, -1, -1);
	nc_init_pair(2, NC_COLOR_WHITE, NC_COLOR_RED);
	nc_init_pair(3, NC_COLOR_WHITE, NC_COLOR_BLUE);
	nc_init_pair(4, NC_COLOR_BLACK, NC_COLOR_GREEN);

	var menuList: list(string) = new list(string);

	menuList.append("ChapelBench " + VERSION);
	menuList.append("by Alexander Senger");
	menuList.append("");
	menuList.append(HardwareInfo.getHardwareInfo());
	menuList.append("");
	menuList.append("Available tests:");
	menuList.append("");
	menuList.append("\t[1] - Matrix-Vector Multiplication");
	menuList.append("\t[2] - Matrix-Matrix Multiplication");
	menuList.append("\t[3] - Prime Number Sieving");
	menuList.append("\t[4] - Prime Factorization");
	menuList.append("\t[5] - Mandlebrot Set");
	menuList.append("");
	menuList.append("\tSequential-only tests:");
	menuList.append("");
	menuList.append("\t[6] - Hyperoperation");
	menuList.append("\t[7] - Ackermann Function");
	menuList.append("");
	menuList.append("[q] - Quit program");

	var scroller = new ScrollArea(menuList);
	var input: int;

	label main while (input != 113) { // "q" is pressed
		nc_curs_set(0);
		nc_noecho();

		scroller.printViewField();
		input = nc_getch();

		select (input) {
			// benchmarks
			when 49 { // "1" is pressed
				if (VectorBench.menu()) {
					break;
				}
			}
			when 50 { // "2" is pressed
				if (MatrixBench.menu()) {
					break;
				}
			}
			when 51 { // "3" is pressed
				if (PrimeSieveBench.menu()) {
					break;
				}
			}
			when 52 { // "4" is pressed
				if (PrimeFactorBench.menu()) {
					break;
				}
			}
			when 53 { // "5" is pressed
				if (MandelbrotBench.menu()) {
					break;
				}
			}
			when 54 { // "6" is pressed
				if (HyperopBench.menu()) {
					break;
				}
			}
			when 55 { // "7" is pressed
				if (AckermannBench.menu()) {
					break;
				}
			}
			when NC_KEY_UP {
				scroller.scrollUp();
			}
			when NC_KEY_DOWN {
				scroller.scrollDown();
			}
		}
	}
	nc_endwin();
}
