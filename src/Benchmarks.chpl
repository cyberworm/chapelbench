module Benchmarks {
	use AckermannBench;
	use HyperopBench;
	use MandelbrotBench;
	use MatrixBench;
	use PrimeFactorBench;
	use PrimeSieveBench;
	use VectorBench;
}
