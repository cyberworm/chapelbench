module CustomContainers {
	use Random;
	use BlockDist;

	/*
		Defines a generic matrix class. A matrix consists of a domain that
		defines its dimension and an array that uses that domain as its
		dimension. Provides basic functions for manipulating the matrix.
	*/
	class Matrix {
		type eltType;
		var dimension: domain(2,int(64),true);
		var data: [dimension] eltType;

		/*
			Constructor that defines the matrix dimension using a number of rows
			and columns. This constructor assumes a starting index of 1 for rows
			and columns.\\

			===== Parameters:
			* type eltType
				** specifies the type of the matrix and its values (e.g. int,
				   real, etc.)
			* rows: int
				** an integer defining the number of rows of the matrix
			* columns: int
				** an integer defining the number of columns of the matrix
		*/
		proc Matrix(type eltType, rows: int, columns: int) {
			this.dimension = {1..rows by 1, 1..columns by 1};
		}

		/*
			Constructor that defines the matrix dimension using a number of rows
			and columns. This constructor assumes a starting index of 1 for rows
			and columns.\\

			===== Parameters:
			* type eltType
				** specifies the type of the matrix and its values (e.g. int,
				   real, etc.)
			* rows: int
				** an integer defining the number of rows of the matrix
			* columns: int
				** an integer defining the number of columns of the matrix
			* defaultValue:
				** initializes the elements of the matrix with a specific value
		*/
		proc Matrix(type eltType, rows: int, columns: int, defaultValue) {
			this.dimension = {1..rows by 1, 1..columns by 1};
			this.data = defaultValue;
		}

		/*
			Constructor that defines the matrix dimension using a domain.\\

			===== Parameters:
			* type eltType
				** specifies the type of the values (e.g. int, real, etc.)
			* dimension: domain(2,int(64),true)
				** a two-dimensional domain that sets the matrix dimension
				   directly
		*/
		proc Matrix(type eltType, dimension: domain(2,int(64),true)) {
			this.dimension = dimension;
		}

		/*
			Constructor that defines the matrix dimension using a domain.\\

			===== Parameters:
			* type eltType
				** specifies the type of the values (e.g. int, real, etc.)
			* dimension: domain(2,int(64),true)
				** a two-dimensional domain that sets the matrix dimension
				   directly
			* defaultValue:
				** initializes the elements of the matrix with a specific value
		*/
		proc Matrix(type eltType, dimension: domain(2,int(64),true), defaultValue) {
			this.dimension = dimension;
			this.data = defaultValue;
		}

		/*
			Returns a specific column of the matrix.\\

			===== Parameters:
			* column: int
				** the column to be returned
			===== Returns:
			* Array: this.eltType
		*/
		proc column(column: int) const {
			return this.data[this.dimension.low[1]..this.dimension.high[1], column];
		}

		/*
			Returns the type of the matrix.\\

			===== Returns:
			* type
		*/
		proc conType type {
			return this.eltType;
		}

		/*
			Returns the dimension of the matrix.\\

			===== Returns:
			* domain(2,int(64),true)
		*/
		proc dim const {
			return this.dimension;
		}

		/*
			Returns the dimension type of the matrix.\\

			===== Returns:
			* type
		*/
		proc dimType type {
			return this.dimension.type;
		}

		/*
			Returns the elements of the matrix.\\

			===== Returns:
			* Array: this.eltType
		*/
		proc elements const {
			return this.data;
		}

		/*
			Does a value based check whether a different matrix equals the
			current one.\\

			===== Parameters:
			* matrix: Matrix
				** the matrix to compare the values with
		*/
		proc equals(ref matrix: Matrix) {
			var equals: bool = true;
			forall (i, j) in zip (this.data, matrix.elements) {
				if (i != j) then equals = false;
			}
			return equals;
		}

		/*
			Fills the matrix with random values between 0 and 1.\\

			===== Note:
			As of the current Chapel version (1.7.0) this only works with
			matrices of type real(64), imag(64) or complex(128)
		*/
		proc insertRandomValues() {
			assert((this.eltType == real(64)) ||
				   (this.eltType == imag(64)) ||
				   (this.eltType == complex(128)),
				   "Matrix not of type real(64), imag(64) or complex(128)");
			Random.fillRandom(this.data);
		}

		/*
			Returns the number of columns of the matrix.\\

			===== Returns:
			* int
		*/
		proc numColumns const {
			return this.dimension.high[2];
		}

		/*
			Returns the number of elements of the matrix.\\

			===== Returns:
			* int
		*/
		proc numElements const {
			return this.dimension.size;
		}

		/*
			Returns the number of rows of the matrix.\\

			===== Returns:
			* int
		*/
		proc numRows const {
			return this.dimension.high[1];
		}

		/*
			Returns a specific row of the matrix.\\

			===== Parameters:
			* row: int
				** the row to be returned
			===== Returns:
			* Array: this.eltType
		*/
		proc row(row: int) const {
			return this.data[row, this.dimension.low[2]..this.dimension.high[2]];
		}

		/*
			Returns a submatrix of the matrix specified by the rows and columns.
			\\

			===== Parameters:
			* rows: range
				** the range of rows to be selected from the matrix. Defaults to
				   all rows.
			* columns: range
				** the range of columns to be selected from the matrix. Defaults
				   to all rows.
			===== Returns:
			* Matrix(this.eltType)
		*/
		proc submatrix(rows: range = this.dimension.low[1]..this.dimension.high[1], columns: range = this.dimension.low[2]..this.dimension.high[2]) {
			var newMat = new Matrix(this.eltType, rows.length, columns.length);
			newMat(1..rows.length, 1..columns.length) = this.data[rows, columns];

			return newMat;
		}

		/*
			Iterates over the matrix elements.\\
		*/
		iter these() {
			for i in this.dimension.dim(1) {
				for j in this.dimension.dim(2) {
					yield this.data[i, j];
				}
			}
		}

		/*
			Getter / Setter method.\\

			===== Parameters:
			* row: int
				** the row index of the element.
			* column: int
				** the column index of the element.
			===== Returns:
			* this.eltType
		*/
		proc this(row: int, column: int) var {
			/*
				These assertions should check whether the given index is within
				range of the domain, but as in Chapel version 1.7.0 assertions
				seem to be horribly leaked, therefore they are deactivated for
				now.
			*/
			// assert((this.dimension.low[1] <= row) && (row <= this.dimension.high[1]),
				   // "Matrix index out-of-bounds: " + row);
			// assert((this.dimension.low[2] <= column) && (column <= this.dimension.high[2]),
				   // "Matrix index out-of-bounds: " + column);
			// assert(this.dimension.dim(1).member(row), "Index not a member of matrix dimension 1:" + row);
			// assert(this.dimension.dim(2).member(column), "Index not a member of matrix dimension 2:" + column);

			return this.data[row, column];
		}

		/*
			Getter / Setter method.\\

			===== Parameters:
			* row: range
				** range of elements over the rows.
			* column: range
				** range of elements over the columns.
			===== Returns:
			* Array: this.eltType
		*/
		proc this(row: range, column: range) var {
			/*
				These assertions should check whether the given index is within
				range of the domain, but as in Chapel version 1.7.0 assertions
				seem to be horribly leaked, therefore they are deactivated for
				now.
			*/
			// assert((this.dimension.low[1] <= row.low) && (row.high <= this.dimension.high[1]),
				   // "Matrix index range out-of-bounds: " + row);
			// assert((this.dimension.low[2] <= column.low) && (column.high <= this.dimension.high[2]),
				   // "Matrix index range out-of-bounds: " + column);
			// assert(this.dimension.dim(1).member(row.low), "Index not a member of matrix dimension 1: " + row.low);
			// assert(this.dimension.dim(1).member(row.high), "Index not a member of matrix dimension 1: " + row.high);
			// assert((this.dimension.stride(1) == row.stride), "Matrix stride mismatch in dimension 1: " + row.stride);
			// assert(this.dimension.dim(2).member(column.low), "Index not a member of matrix dimension 2: " + column.low);
			// assert(this.dimension.dim(2).member(column.high), "Index not a member of matrix dimension 2: " + column.high);
			// assert((this.dimension.stride(2) == column.stride), "Matrix stride mismatch in dimension 2: " + column.stride);

			return this.data[row, column];
		}
	}

	/*
		Defines a generic matrix class. A matrix consists of a domain that
		defines its dimension and an array that uses that domain as its
		dimension. Provides basic functions for manipulating the matrix. This
		matrix class uses a block domain map for the array of elements. There
		might be different matrix classes using other domain maps in the future.
	*/
	class DistMatrix {
		type eltType;
		var dimension: domain(2,int(64),true) dmapped Block({1..1, 1..1});
		var data: [dimension] eltType;

		/*
			Constructor that defines the matrix dimension using a number of row
			and columns.\\

			===== Parameters:
			* type eltType
				** specifies the type of the matrix and its values (e.g. int,
				   real, etc.)
			* rows: int
				** an integer defining the number of rows of the matrix
			* columns: int
				** an integer defining the number of columns of the matrix
		*/
		proc DistMatrix(type eltType, rows: int, columns: int) {
			this.dimension = {1..rows by 1, 1..columns by 1} dmapped Block({1..rows, 1..columns});
		}

		/*
			Constructor that defines the matrix dimension using a number of row
			and columns.\\

			===== Parameters:
			* type eltType
				** specifies the type of the matrix and its values (e.g. int,
				   real, etc.)
			* rows: int
				** an integer defining the number of rows of the matrix
			* columns: int
				** an integer defining the number of columns of the matrix
			* defaultValue:
				** initializes the elements of the matrix with a specific value
		*/
		proc DistMatrix(type eltType, rows: int, columns: int, defaultValue) {
			this.dimension = {1..rows by 1, 1..columns by 1} dmapped Block({1..rows, 1..columns});
			this.data = defaultValue;
		}

		/*
			Constructor that defines the matrix dimension using a domain.\\

			===== Parameters:
			* type eltType
				** specifies the type of the values (e.g. int, real, etc.)
			* dimension: domain(2,int(64),true)
				** a two-dimensional domain that sets the matrix dimension
				   directly
		*/
		proc DistMatrix(type eltType, dimension: domain(2,int(64),true)) {
			this.dimension = dimension dmapped Block({dimension.low[1]..dimension.high[1], dimension.low[2]..dimension.high[2]});
		}

		/*
			Constructor that defines the matrix dimension using a domain.\\

			===== Parameters:
			* type eltType
				** specifies the type of the values (e.g. int, real, etc.)
			* dimension: domain(2,int(64),true)
				** a two-dimensional domain that sets the matrix dimension
				   directly
			* defaultValue:
				** initializes the elements of the matrix with a specific value
		*/
		proc DistMatrix(type eltType, dimension: domain(2,int(64),true), defaultValue) {
			this.dimension = dimension dmapped Block({dimension.low[1]..dimension.high[1], dimension.low[2]..dimension.high[2]});
			this.data = defaultValue;
		}

		/*
			Returns a specific column of the matrix.\\

			===== Parameters:
			* column: int
				** the column to be returned
			===== Returns:
			* Array: this.eltType
		*/
		proc column(column: int) const {
			return this.data[this.dimension.low[1]..this.dimension.high[1], column];
		}

		/*
			Returns the type of the matrix.\\

			===== Returns:
			* type
		*/
		proc conType type {
			return this.eltType;
		}

		/*
			Returns the dimension of the matrix.\\

			===== Returns:
			* domain(2,int(64),true)
		*/
		proc dim const {
			return this.dimension;
		}

		/*
			Returns the dimension type of the matrix.\\

			===== Returns:
			* type
		*/
		proc dimType type {
			return this.dimension.type;
		}

		/*
			Returns the elements of the matrix.\\

			===== Returns:
			* Array: this.eltType
		*/
		proc elements const {
			return this.data;
		}

		/*
			Does a value based check whether a different matrix equals the
			current one.\\

			===== Parameters:
			* matrix: Matrix
				** the matrix to compare the values with
		*/
		proc equals(ref matrix: DistMatrix) {
			var equals: bool = true;
			forall (i, j) in zip (this.data, matrix.elements) {
				if (i != j) then equals = false;
			}
			return equals;
		}

		/*
			Fills the matrix with random values between 0 and 1.\\

			===== Note:
			As of the current Chapel version (1.7.0) this only works with
			matrices of type real(64), imag(64) or complex(128)
		*/
		proc insertRandomValues() {
			assert((this.eltType == real(64)) ||
				   (this.eltType == imag(64)) ||
				   (this.eltType == complex(128)),
				   "Matrix not of type real(64), imag(64) or complex(128)");
			Random.fillRandom(this.data);
		}

		/*
			Returns the number of columns of the matrix.\\

			===== Returns:
			* int
		*/
		proc numColumns const {
			return this.dimension.high[2];
		}

		/*
			Returns the number of elements of the matrix.\\

			===== Returns:
			* int
		*/
		proc numElements const {
			return this.dimension.size;
		}

		/*
			Returns the number of rows of the matrix.\\

			===== Returns:
			* int
		*/
		proc numRows const {
			return this.dimension.high[1];
		}

		/*
			Returns a specific row of the matrix.\\

			===== Parameters:
			* row: int
				** the row to be returned
			===== Returns:
			* Array: this.eltType
		*/
		proc row(row: int) const {
			return this.data[row, this.dimension.low[2]..this.dimension.high[2]];
		}

		/*
			Returns a submatrix of the matrix specified by the rows and columns.
			\\

			===== Parameters:
			* rows: range
				** the range of rows to be selected from the matrix. Defaults to
				   all rows.
			* columns: range
				** the range of columns to be selected from the matrix. Defaults
				   to all rows.
			===== Returns:
			* DistMatrix(this.eltType)
		*/
		proc submatrix(rows: range = this.dimension.low[1]..this.dimension.high[1], columns: range = this.dimension.low[2]..this.dimension.high[2]) {
			var newMat = new DistMatrix(this.eltType, rows.length, columns.length);
			newMat(1..rows.length, 1..columns.length) = this.data[rows, columns];

			return newMat;
		}

		/*
			Iterates over the matrix elements.\\
		*/
		iter these() {
			for i in this.dimension.dim(1) {
				for j in this.dimension.dim(2) {
					yield this.data[i, j];
				}
			}
		}

		/*
			Getter / Setter method.\\

			===== Parameters:
			* row: int
				** the row index of the element.
			* column: int
				** the column index of the element.
			===== Returns:
			* this.eltType
		*/
		proc this(row: int, column: int) var {
			/*
				These assertions should check whether the given index is within
				range of the domain, but as in Chapel version 1.7.0 assertions
				seem to be horribly leaked, therefore they are deactivated for
				now.
			*/
			// assert((this.dimension.low[1] <= row) && (row <= this.dimension.high[1]),
				   // "Matrix index out-of-bounds: " + row);
			// assert((this.dimension.low[2] <= column) && (column <= this.dimension.high[2]),
				   // "Matrix index out-of-bounds: " + column);
			// assert(this.dimension.dim(1).member(row), "Index not a member of matrix dimension 1:" + row);
			// assert(this.dimension.dim(2).member(column), "Index not a member of matrix dimension 2:" + column);

			return this.data[row, column];
		}

		/*
			Getter / Setter method.\\

			===== Parameters:
			* row: range
				** range of elements over the rows.
			* column: range
				** range of elements over the columns.
			===== Returns:
			* Array: this.eltType
		*/
		proc this(row: range, column: range) var {
			/*
				These assertions should check whether the given index is within
				range of the domain, but as in Chapel version 1.7.0 assertions
				seem to be horribly leaked, therefore they are deactivated for
				now.
			*/
			// assert((this.dimension.low[1] <= row.low) && (row.high <= this.dimension.high[1]),
				   // "Matrix index range out-of-bounds: " + row);
			// assert((this.dimension.low[2] <= column.low) && (column.high <= this.dimension.high[2]),
				   // "Matrix index range out-of-bounds: " + column);
			// assert(this.dimension.dim(1).member(row.low), "Index not a member of matrix dimension 1: " + row.low);
			// assert(this.dimension.dim(1).member(row.high), "Index not a member of matrix dimension 1: " + row.high);
			// assert((this.dimension.stride(1) == row.stride), "Matrix stride mismatch in dimension 1: " + row.stride);
			// assert(this.dimension.dim(2).member(column.low), "Index not a member of matrix dimension 2: " + column.low);
			// assert(this.dimension.dim(2).member(column.high), "Index not a member of matrix dimension 2: " + column.high);
			// assert((this.dimension.stride(2) == column.stride), "Matrix stride mismatch in dimension 2: " + column.stride);

			return this.data[row, column];
		}
	}

	/*
		Defines a generic vector class. A vector consists of
		a domain that defines its dimension and an array of elments that uses
		that domain as its dimension.
		Provides basic functions for manipulating the vector.
	*/
	class Vector {
		type eltType;
		var dimension: domain(1,int(64),true);
		var data: [dimension] eltType;

		/*
			Constructor that defines the vector dimension using an integer. This
			constructor assumes a starting index of 1.\\

			===== Parameters:
			* type eltType
				** specifies the type of the vector and its values (e.g. int,
				real, etc.)
			* length: int
				** an integer defining the length of the vector
		*/
		proc Vector(type eltType, length: int) {
			this.dimension = {1..length by 1};
		}

		/*
			Constructor that defines the vector dimension using an integer. This
			constructor assumes a starting index of 1.\\

			===== Parameters:
			* type eltType
				** specifies the type of the vector and its values (e.g. int,
				real, etc.)
			* length: int
				** an integer defining the length of the vector
			* defaultValue:
				** initializes the elements of the vector with a specific value
		*/
		proc Vector(type eltType, length: int, defaultValue) {
			this.dimension = {1..length by 1};
			this.data = defaultValue;
		}

		/*
			Constructor that defines the vector dimension using a domain.\\

			===== Parameters:
			* type eltType
				** specifies the type of the values (e.g. int, real, etc.)
			* dimension: domain(1,int(64),true)
				** a one-dimensional domain that sets the vector dimension
				   directly
		*/
		proc Vector(type eltType, dimension: domain(1,int(64),true)) {
			this.dimension = dimension;
		}

		/*
			Constructor that defines the vector dimension using a domain.\\

			===== Parameters:
			* type eltType
				** specifies the type of the values (e.g. int, real, etc.)
			* dimension: domain(1,int(64),true)
				** a one-dimensional domain that sets the vector dimension
				   directly
			* defaultValue:
				** initializes the elements of the vector with a specific value
		*/
		proc Vector(type eltType, dimension: domain(1,int(64),true), defaultValue) {
			this.dimension = dimension;
			this.data = defaultValue:this.eltType;
		}
		/*
			Returns the type of the vector.\\

			===== Returns:
			* type
		*/
		proc conType type {
			return this.eltType;
		}

		/*
			Returns the dimension of the vector.\\

			===== Returns:
			* domain(1,int(64),true)
		*/
		proc dim const {
			return this.dimension;
		}

		/*
			Returns the dimension type of the vector.\\

			===== Returns:
			* type
		*/
		proc dimType type {
			this.dimension.type;
		}

		/*
			Returns the elements of the vector.\\

			===== Returns:
			* Array: this.eltType
		*/
		proc elements const {
			return this.data;
		}

		/*
			Does a value based check whether a different vector equals the
			current one.\\

			===== Parameters:
			* vector: Vector
				** the vector to compare the values with
		*/
		proc equals(vector: Vector) {
			var equals: bool = true;
			forall (i, j) in zip (this.data, vector.elements) {
				if (i != j) then equals = false;
			}
			return equals;
		}

		/*
			Fills the vector with random values between 0 and 1.\\

			===== Note:
			As of the current Chapel version (1.7.0) this only works with
			vectors of type real(64), imag(64) or complex(128)
		*/
		proc insertRandomValues() {
			assert((this.eltType == real(64)) ||
				   (this.eltType == imag(64)) ||
				   (this.eltType == complex(128)),
				   "Vector not of type real(64), imag(64) or complex(128)");
			Random.fillRandom(this.data);
		}

		/*
			Returns the number of elements of the vector.\\

			===== Returns:
			* int
		*/
		proc numElements const {
			return this.dimension.size;
		}

		/*
			Iterates over the vector elements
		*/
		iter these() {
			for i in this.dimension.dim(1) {
				yield this.data[i];
			}
		}

		/*
			Getter / Setter method.\\

			===== Parameters:
			* idx: int
				** the index of the accessed element
		*/
		proc this(idx: int) var {
			/*
				These assertions should check whether the given index is within
				range of the domain, but as in Chapel version 1.7.0 assertions
				seem to be horribly leaked, therefore they are deactivated for
				now.
			*/
			// assert((this.dimension.low <= idx) && (idx <= this.dimension.high),
				   // "Vector index out-of-bounds: " + idx);
			// assert(this.dimension.member(idx), "Index not a member of vector dimension: " + idx);

			return this.data[idx];
		}

		/*
			Getter / Setter method\\.

			===== Parameters:
			* idx: range
				** range of elements over the vector
		*/
		proc this(idx: range) var {
			/*
				These assertions should check whether the given index is within
				range of the domain, but as in Chapel version 1.7.0 assertions
				seem to be horribly leaked, therefore they are deactivated for
				now.
			*/
			// assert((this.dimension.low <= idx.low) && (idx.high <= this.dimension.high),
				   // "Vector index range out-of-bounds: " + idx);
			// assert(this.dimension.member(idx.low), "Index not a member of vector dimension: " + idx.low);
			// assert(this.dimension.member(idx.high), "Index not a member of vector dimension: " + idx.high);
			// assert((this.dimension.stride == idx.stride), "Vector stride mismatch: " + idx.stride);

			return this.data[idx];
		}
	}

	/*
		Defines a generic vector class. A vector consists of
		a domain that defines its dimension and an array of elments that uses
		that domain as its dimension. Provides basic functions for manipulating
		the vector.This vector class uses a block domain map for the vector
		elements. There might be different vector classes using other domain
		maps in the future.
	*/
	class DistVector {
		type eltType;
		var dimension: domain(1,int(64),true) dmapped Block({1..1});
		var data: [dimension] eltType;

		/*
			Constructor that defines the vector dimension using an integer. This
			constructor assumes a starting index of 1.\\

			===== Parameters:
			* type eltType
				** specifies the type of the vector and its values (e.g. int,
				real, etc.)
			* length: int
				** an integer defining the length of the vector
		*/
		proc DistVector(type eltType, length: int) {
			this.dimension = {1..length by 1} dmapped Block({1..length});
		}

		/*
			Constructor that defines the vector dimension using an integer. This
			constructor assumes a starting index of 1.\\

			===== Parameters:
			* type eltType
				** specifies the type of the vector and its values (e.g. int,
				real, etc.)
			* length: int
				** an integer defining the length of the vector
			* defaultValue:
				** initializes the elements of the vector with a specific value
		*/
		proc DistVector(type eltType, length: int, defaultValue) {
			this.dimension = {1..length by 1} dmapped Block({1..length});
			this.data = defaultValue;
		}

		/*
			Constructor that defines the vector dimension using a domain.\\

			===== Parameters:
			* type eltType
				** specifies the type of the values (e.g. int, real, etc.)
			* dimension: domain(1,int(64),true)
				** a one-dimensional domain that sets the vector dimension
				   directly
		*/
		proc DistVector(type eltType, dimension: domain(1,int(64),true)) {
			this.dimension = dimension dmapped Block({dimension.low..dimension.high});
		}

		/*
			Constructor that defines the vector dimension using a domain.\\

			===== Parameters:
			* type eltType
				** specifies the type of the values (e.g. int, real, etc.)
			* dimension: domain(1,int(64),true)
				** a one-dimensional domain that sets the vector dimension
				   directly
			* defaultValue:
				** initializes the elements of the vector with a specific value
		*/
		proc DistVector(type eltType, dimension: domain(1,int(64),true), defaultValue) {
			this.dimension = dimension dmapped Block({dimension.low..dimension.high});
			this.data = defaultValue;
		}

		/*
			Returns the type of the vector.\\

			===== Returns:
			* type
		*/
		proc conType type {
			return this.eltType;
		}

		/*
			Returns the dimension of the vector.\\

			===== Returns:
			* domain(1,int(64),true)
		*/
		proc dim const {
			return this.dimension;
		}

		/*
			Returns the dimension type of the vector.\\

			===== Returns:
			* type
		*/
		proc dimType type {
			return this.dimension.type;
		}

		/*
			Returns the elements of the vector.\\

			===== Returns:
			* Array: this.eltType
		*/
		proc elements const {
			return this.data;
		}

		/*
			Does a value based check whether a different vector equals the
			current one.\\

			===== Parameters:
			* vector: DistVector
				** the vector to compare the values with
		*/
		proc equals(vector: DistVector) {
			var equals: bool = true;
			forall (i, j) in zip (this.data, vector.elements) {
				if (i != j) then equals = false;
			}
			return equals;
		}

		/*
			Fills the vector with random values between 0 and 1.\\

			===== Note:
			As of the current Chapel version (1.7.0) this only works with
			vectors of type real(64), imag(64) or complex(128)
		*/
		proc insertRandomValues() {
			assert((this.eltType == real(64)) ||
				   (this.eltType == imag(64)) ||
				   (this.eltType == complex(128)),
				   "Vector not of type real(64), imag(64) or complex(128)");
			Random.fillRandom(this.data);
		}

		/*
			Returns the number of elements of the vector.\\

			===== Returns:
			* int
		*/
		proc numElements const {
			return this.dimension.size;
		}

		/*
			Iterates over the vector elements
		*/
		iter these() {
			for i in this.dimension.dim(1) {
				yield this.data[i];
			}
		}

		/*
			Getter / Setter method.\\

			===== Parameters:
			* idx: int
				** the index of the accessed element
		*/
		proc this(idx: int) var {
			/*
				These assertions should check whether the given index is within
				range of the domain, but as in Chapel version 1.7.0 assertions
				seem to be horribly leaked, therefore they are deactivated for
				now.
			*/
			// assert((this.dimension.low <= idx) && (idx <= this.dimension.high),
				   // "Vector index out-of-bounds: " + idx);
			// assert(this.dimension.member(idx), "Index not a member of vector dimension: " + idx);

			return this.data[idx];
		}

		/*
			Getter / Setter method\\.

			===== Parameters:
			* idx: range
				** range of elements over the vector
		*/
		proc this(idx: range) var {
			/*
				These assertions should check whether the given index is within
				range of the domain, but as in Chapel version 1.7.0 assertions
				seem to be horribly leaked, therefore they are deactivated for
				now.
			*/
			// assert((this.dimension.low <= idx.low) && (idx.high <= this.dimension.high),
				   // "Vector index range out-of-bounds: " + idx);
			// assert(this.dimension.member(idx.low), "Index not a member of vector dimension: " + idx.low);
			// assert(this.dimension.member(idx.high), "Index not a member of vector dimension: " + idx.high);
			// assert((this.dimension.stride == idx.stride), "Vector stride mismatch: " + idx.stride);

			return this.data[idx];
		}
	}

	/*
		Main routine for the class to test its functionality
	*/
	proc main() {
		use CustomContainers;
		var matD = new Matrix(real, 5, 5);
		matD.insertRandomValues();
		writeln("matD elements:\n", matD.elements);
		writeln("matD dimension:\n", matD.dim);
		writeln("matD element at (2,4):\n", matD(2,4));
		writeln("matD type:\n", typeToString(matD.conType));
		writeln("matD # columns:\n", matD.numColumns);
		writeln("matD # rows:\n", matD.numRows);

		writeln("\nSet value at (1,3) to 2.5:\n");
		matD(1,3) = 2.5;
		writeln("matD elements:\n", matD.elements);
		writeln("matD first row:\n", matD.row(1));
		writeln("matD second column:\n", matD.column(2));

		writeln("\nCreate submatrix matC from third and fourth row of matrix matD:\n");
		var matC = matD.submatrix(3..5);
		writeln("matC elements:\n", matC.elements);
		writeln("matC height:\n", matC.numRows);
		writeln("matC dimension:\n", matC.dim);

		writeln("\nCreate submatrix matB from first two columns of matrix matD:\n");
		var matB = matD.submatrix(columns=1..2);
		writeln("matB elements:\n", matB.elements);
		writeln("matB dimension:\n", matB.dim);

		writeln("\nCreate submatrix matA from fourth and fifth rows / columns of matrix matD:\n");
		var matA = matD.submatrix(4..5, 4..5);
		writeln("matA elements:\n", matA.elements);
		writeln("matA dimension:\n", matA.dim);

		delete matD;
		delete matC;
		delete matB;
		delete matA;
	}
}
