/*
	This module contains the matrix-matrix multiplication benchmark and
	its needed functions. The benchmark supports sequential and parallel
	computation and basic measuring. For now only two square matrices will be
	used.
*/
module MatrixBench {
	use Helpers;
	use Classes.CustomContainers;

	// General options
	var autoExit: bool = false;
	var distributed: bool = false;
	var logFile: bool = true;
	var testMode: int = 3; // possible values from 1..3, see enum modeText below
	var fileName: string = "ChapelBench_MatrixMatrixMul_" +
						   Formatting.getFormattedDate("") +
						   "_" + Formatting.getFormattedTime("");
	var numRuns: uint = 3;
	enum boolText { no, yes }
	enum modeText { sequential, parallel, both }

	// Benchmark-specific options
	var dimension: uint = 640;


	/*
		Calculates and returns the floating point operation per seconds of a
		matrix-matrix multiplication.\\

		===== Parameters:
		* matA
			** the first matrix (Matrix or DistMatrix) used in the
			   multiplication
		* matB
			** the first matrix (Matrix or DistMatrix) used in the
			   multiplication
	   * time: real
			** a floating point number representing the elapsed time (in
			   seconds) of the multiplication
		===== Returns:
		* real
	*/
	proc calcMegaFLOPS(ref matA, ref matB, time: real) {
		/*
			for two nxn type matrices an easy way to calcualte flops is
			2*n^3-n^2:
				var FLOPS: int = 2*(dimension**3)-(dimension**2);
			but for mxn and nxp matrices this formular is needed: m*p*(2*n-1)
			TODO: let the user choose the unit of the result: FLOPS, kFLOPS, etc
		*/
		var FLOPS: int;
		FLOPS = matA.numRows * matB.numColumns * (2 * matA.numColumns - 1);
		var kFLOPS: real = FLOPS / 1000;
		var MFLOPS: real = kFLOPS / 1000;
		return MFLOPS / time;
	}

	/*
		Concatinates the rows of multiple matrices and returns a new matrix.
		For example: 3x2 matrix + 7x2 matrix = 10x2 matrix.\\

		===== Parameters:
		* matrices: [] Matrix(real)
			** an array holding the matrices to be concatenated
		===== Returns:
		* Matrix(real)
	*/
	proc concat(ref matrices: [] Matrix(real)) {
		// make sure the number of columns are equal on all matrices in the array
		assert(matrices[1].numColumns == [i in matrices] i.numColumns, "Number of columns don't match");
		var totalRows: int = + reduce ([i in matrices] i.numRows);

		var newElements: [1..totalRows, 1..matrices[1].numColumns] real;

		var start: int = 1;
		var end: int;

		// iterate over the array, extract the elements from each matrix and
		// insert them into the array for the new matrix
		for matrix in matrices {
			end = matrix.numRows + start - 1;
			newElements[start..end, 1..] = matrix.elements;
			start = end+1;
		}

		var newMat: Matrix(real) = new Matrix(real, totalRows, matrices[1].numColumns);
		for i in newElements.domain.dim(1) {
			for j in newElements.domain.dim(2) {
				newMat(i, j) = newElements[i, j];
			}
		}

		return newMat;
	}

	/*
		Concatinates the rows of multiple matrices and returns a new matrix.
		For example: 3x2 matrix + 7x2 matrix = 10x2 matrix.\\

		===== Parameters:
		* matrices: [] Matrix(real)
			** an array holding the matrices to be concatenated
		===== Returns:
		* Matrix(real)
	*/
	proc concatDistributed(ref matrices: [] DistMatrix(real)) {
		// make sure the number of columns are equal on all matrices in the array
		assert(matrices[1].numColumns == [i in matrices] i.numColumns, "Number of columns don't match");
		var totalRows: int = + reduce ([i in matrices] i.numRows);

		var newElements: [1..totalRows, 1..matrices[1].numColumns] real;

		var start: int = 1;
		var end: int;

		// iterate over the array, extract the elements from each matrix and
		// insert them into the array for the new matrix
		for matrix in matrices {
			end = matrix.numRows + start - 1;
			newElements[start..end, 1..] = matrix.elements;
			start = end+1;
		}

		var newMat: DistMatrix(real) = new DistMatrix(real, totalRows, matrices[1].numColumns);
		for i in newElements.domain.dim(1) {
			for j in newElements.domain.dim(2) {
				newMat(i, j) = newElements[i, j];
			}
		}

		return newMat;
	}

	/*
		Returns the product of two matrices which has been calculated in
		parallel and the elapsed time.\\

		===== Parameters:
		* A: Matrix(real)
			** the first factor of the product
		* B: Matrix(real)
			** the second factor of the product
		===== Returns:
		* (Matrix(real), real)
	*/
	proc parallelMultiply(ref A: Matrix(real), ref B: Matrix(real)) {
	/*
		Calculation is done by splitting one matrix as equally as possible into
		smaller submatrices along its rows and distribute them to a number of
		tasks (which corresponds with the number of available cores), so that
		each thread calculates a fragment of the result in serial. After each
		thread is finished the resulting submatrices are being concatenated to
		form the resulting matrix.
	*/
		var cores: int = HardwareInfo.getNumCores();
		//  task syncronization variables
		var count$: sync int = cores;
		var release$: single bool;

		var t: Timer;

		var input: [1..cores] Matrix(real);
		var output: [1..cores] Matrix(real);

		var step: int = A.numRows / cores;
		var start: int = 1;
		var end: int;

		// split matrix A into several submatrices
		for i in 1..cores {
			end = step * i;
			if (i == cores) then end = A.numRows;
			input[i] = A.submatrix(start..end);
			start = end + 1;
		}

		// begin of the calculation in parallel
		t.start();
		forall i in 1..cores {
			/*
				Instead of using a forall-loop and the syncronization variables
				the 'sync' keyword could have been used in a normal for-loop,
				but this method proofed to be more efficient.
			*/
			output[i] = sequentialMultiply(input[i], B)(1);

			// set the state of count$ to empty to block other tasks
			var remainingTasks: int = count$;

			if (remainingTasks != 1) {
				// set the state of count$ to full so other tasks can proceed
				count$ = remainingTasks-1;
				// all but the last task will wait here until release$ is assigned
				release$;
			} else {
				// the last task will reach this point and assign the variable
				release$ = true;
			}
		}
		t.stop();

		// concat the resulting submatrices
		var newMat: Matrix(real) = concat(output);

		// delete the initial and resulting submatrices to free memory
		for (i, j) in zip (input, output) {
			delete i;
			delete j;
		}

		return (newMat, t.elapsed());
	}

	/*
		Returns the product of two matrices which has been calculated in
		parallel and the elapsed time; using distributed memory.\\

		===== Parameters:
		* A: DistMatrix(real)
			** the first factor of the product
		* B: DistMatrix(real)
			** the second factor of the product
		===== Returns:
		* (DistMatrix(real), real)
	*/
	proc parallelMultiplyDistributed(ref A: DistMatrix, ref B: DistMatrix) {
	/*
		Calculation is done by splitting one matrix as equally as possible into
		smaller submatrices along its rows and distribute them to a number of
		tasks (which corresponds with the number of available cores), so that
		each thread calculates a fragment of the result in serial. After each
		thread is finished the resulting submatrices are being concatenated to
		form the resulting matrix.
	*/
		var cores: int = HardwareInfo.getNumCores();
		//  task syncronization variables
		var count$: sync int = cores;
		var release$: single bool;

		var t: Timer;

		var input: [1..cores] DistMatrix(real);
		var output: [1..cores] DistMatrix(real);

		var step: int = A.numRows / cores;
		var start: int = 1;
		var end: int;

		// split matrix A into several submatrices
		for i in 1..cores {
			end = step * i;
			if (i == cores) then end = A.numRows;
			input[i] = A.submatrix(start..end);
			start = end + 1;
		}

		// begin of the calculation in parallel
		t.start();
		forall i in 1..cores {
			output[i] = sequentialMultiplyDistributed(input[i], B)(1);

			// set the state of count$ to empty to block other tasks
			var remainingTasks: int = count$;

			if (remainingTasks != 1) {
				// set the state of count$ to full so other tasks can proceed
				count$ = remainingTasks-1;
				// all but the last task will wait here until release$ is assigned
				release$;
			} else {
				// the last task will reach this point and assign the variable
				release$ = true;
			}
		}
		t.stop();

		// concat the resulting submatrices
		var newMat: DistMatrix(real) = concatDistributed(output);

		// delete the initial and resulting submatrices to free memory
		for (i, j) in zip (input, output) {
			delete i;
			delete j;
		}

		return (newMat, t.elapsed());
	}

	/*
		Returns the product of two matrices and the elapsed time.\\

		===== Parameters:
		* A: Matrix(real)
			** the first factor of the product
		* B: Matrix(real)
			** the second factor of the product
		===== Returns:
		* (Matrix(real), real)
	*/
	proc sequentialMultiply(ref A: Matrix, ref B: Matrix) {
		assert(A.numColumns == B.numRows, "Number of columns of matrix A has to match number of rows of matrix B");
		var result: Matrix(real) = new Matrix(real, A.numRows, B.numColumns);

		var value: real;
		var t: Timer;

		t.start();
		for row in 1..A.numRows {
			for column in 1..B.numColumns {
				value = 0;
				for (rowElement, columnElement) in zip (A.row(row), B.column(column)) {
					value += rowElement * columnElement;
				}
				result(row, column) = value;
			}
		}
		t.stop();

		return (result, t.elapsed());
	}

	/*
		Returns the product of two matrices and the elapsed time; using
		distributed memory.\\

		===== Parameters:
		* A: DistMatrix(real)
			** the first factor of the product
		* B: DistMatrix(real)
			** the second factor of the product
		===== Returns:
		* (DistMatrix(real), real)
	*/
	proc sequentialMultiplyDistributed(ref A: DistMatrix, ref B: DistMatrix) {
		assert(A.numColumns == B.numRows, "Number of columns of matrix A has to match number of rows of matrix B");
		var result: DistMatrix(real) = new DistMatrix(real, A.numRows, B.numColumns);

		var value: real;
		var t: Timer;

		t.start();
		for row in 1..A.numRows {
			for column in 1..B.numColumns {
				value = 0;
				for (rowElement, columnElement) in zip (A.row(row), B.column(column)) {
					value += rowElement * columnElement;
				}
				result(row, column) = value;
			}
		}
		t.stop();

		return (result, t.elapsed());
	}

	/*
		Displays the main menu of the benchmark. Returns a boolean value to
		determine whether the application should be exited. True will be
		returned if autoExit is set or when the user requested to quit
		the program at the post test query.\\
	*/
	proc menu() {
		/*
			Refreshes the menu to display changes in the options.\\
		*/
		proc refresh() {
			nc_clear();
			nc_color_set(1);

			var menuList: list(string) = new list(string);

			menuList.append("Matrix-Matrix Multiplication");
			menuList.append("");
			menuList.append("Calculates the product of two matrices. Calculation");
			menuList.append("time and memory usage grows exponentially with the");
			menuList.append("dimension of the matrices.");
			menuList.append("");
			menuList.append("\t[v] - Matrix size: " + Formatting.getFormattedNumber(dimension));
			menuList.append("");
			menuList.append("\t[q] - Return to main menu");
			menuList.append("\t[s] - Start benchmark");
			menuList.append("");
			menuList.append("Options:");
			menuList.append("");
			menuList.append("\t[a] - Automatically quit program: " + (autoExit + 1):boolText);
			menuList.append("\t[d] - Use distributed memory: " + (distributed + 1):boolText);
			menuList.append("\t[l] - Create log file: " + (logFile + 1):boolText);
			menuList.append("\t[m] - Test mode: " + testMode:modeText);
			menuList.append("\t[n] - Log file name: " + fileName + ".log");
			menuList.append("\t[r] - Number of test runs: " + Formatting.getFormattedNumber(numRuns));

			return menuList;
		}

		var menuList = refresh();
		var scroller = new ScrollArea(menuList);
		var input: int;

		label main while (input != 113) { // "q" is pressed
			nc_curs_set(0);
			nc_noecho();

			menuList = refresh();
			scroller.setData(menuList);
			scroller.printViewField();
			input = nc_getch();

			select (input) {
				when 97 { // "a" is pressed
					autoExit = !autoExit;
				}
				when 100 { // "d" is pressed
					if (HardwareInfo.getNumCPUs() > 1) {
						distributed = !distributed;
					} else {
						nc_color_set(2);
						nc_mvaddstr(NC_SCROLL_LINES+2, 0, "This option requires at least two CPUs!");
						nc_getch();
					}
				}
				when 108 { // "l" is pressed
					logFile = !logFile;
				}
				when 109 { // "m" is pressed
					testMode = (testMode % 3) + 1;
				}
				when 110 { // "n" is pressed
					fileName = Interface.setLogFileName("MatrixMatrixMul");
				}
				when 114 { // "r" is pressed
					numRuns = Interface.setRuns();
				}
				when 115 { // "s" is pressed
					var result: (string, list(string)) = MatrixBench.run({1..dimension:int, 1..dimension:int}, {1..dimension:int, 1..dimension:int});

					if (autoExit) {
						result(2).destroy();
						return true;
					}
					if (Interface.postTestQuery(result(1), result(2))) {
						return true;
					}
				}
				when 118 { // "v" is pressed
					dimension = Interface.setUnsignedValue("Enter dimension (rows = columns):");
					if (dimension == 0) {
						dimension = 640;
					}
				}
				when NC_KEY_UP {
					scroller.scrollUp();
				}
				when NC_KEY_DOWN {
					scroller.scrollDown();
				}
			}
		}
		return false;
	}

	/*
		Runs the matrix-matrix multiplication benchmark and returns a
		tuple containing the path/name of the log file and a list of strings
		containing the displayed text.\\

		===== Parameters:
		* dimA: domain(2)
			** the dimension of matrix A
		* dimB: domain(2)
			** the dimension of matrix B
		===== Returns:
		* (string, list(string))
	*/
	proc run(dimA: domain(2), dimB: domain(2)) {
		nc_clear();
		nc_curs_set(0);
		var logger: Logger;
		// if logging is disabled at least create a log file in the OS's temporary folder for debugging
		if (logFile) {
			logger = new Logger(fileName + ".log");
		} else {
			logger = new Logger();
		}

		logger.log("CPU " + getCPUID() + ":", buffered = false);
		logger.log("\tCores: " + getNumCores(), buffered = false);
		logger.log("\tAvailable Memory: " + getMemory(MemUnits.MB) + " MB", buffered = false);

		logger.log(nc_rmvaddstrln(0, 0, "Matrix-Matrix Multiplication details:"));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tDimension matrix A: " + Formatting.getFormattedNumber(dimA.high[1]) + "x" + Formatting.getFormattedNumber(dimA.high[2])));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tDimension matrix B: " + Formatting.getFormattedNumber(dimB.high[1]) + "x" + Formatting.getFormattedNumber(dimB.high[2])));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tUsing distributed memory: " + (distributed + 1):boolText));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tTest mode: " + testMode:modeText));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tNumber of test runs: " + Formatting.getFormattedNumber(numRuns)));

		var sequentialFLOPS: real;
		var parallelFLOPS: real;
		var speedup: real;
		var efficiency: real;

		// used for the calculation of the respective average
		var sumSequentialTimes: real;
		var sumParallelTimes: real;
		var sumSequentialFLOPS: real;
		var sumParallelFLOPS: real;
		var sumSpeedups: real;
		var sumEfficiencies: real;

		if (distributed) {
			for run in 1..numRuns {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tRun " + run + " of " + Formatting.getFormattedNumber(numRuns) + ":"));
				logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tSetting up matrices..."));
				var matA: DistMatrix(real) = new DistMatrix(real, dimA by 1);
				matA.insertRandomValues();
				var matB: DistMatrix(real) = new DistMatrix(real, dimB by 1);
				matB.insertRandomValues();
				nc_addstrln(" done");

				var sequentialResult: (DistMatrix(real), real);
				var parallelResult: (DistMatrix(real), real);

				if (testMode == 1 || testMode == 3) {
					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin sequential multiplication..."));
					sequentialResult = sequentialMultiplyDistributed(matA, matB);
					nc_addstrln(" done");
					sumSequentialTimes += sequentialResult(2);
					sequentialFLOPS = calcMegaFLOPS(matA, matB, sequentialResult(2));
					sumSequentialFLOPS += sequentialFLOPS;
				}

				if (testMode == 2 || testMode == 3) {
					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin parallel multiplication..."));
					parallelResult = parallelMultiplyDistributed(matA, matB);
					nc_addstrln(" done");
					sumParallelTimes += parallelResult(2);
					parallelFLOPS = calcMegaFLOPS(matA, matB, parallelResult(2));
					sumParallelFLOPS += parallelFLOPS;
				}

				if (testMode == 3) {
					speedup = Statistics.speedup(sequentialResult(2), parallelResult(2));
					efficiency = Statistics.efficiency(sequentialResult(2), parallelResult(2), HardwareInfo.getNumCores());
					sumSpeedups += speedup;
					sumEfficiencies += efficiency;
				}

				if (testMode == 1 || testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSequential time: " + Formatting.getFormattedNumber(sequentialResult(2)) + " seconds"));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSequential FLOPS: " + Formatting.getFormattedNumber(sequentialFLOPS) + " MFLOPS"));
				}
				if (testMode == 2 || testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel time: " + Formatting.getFormattedNumber(parallelResult(2)) + " seconds"));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel FLOPS: " + Formatting.getFormattedNumber(parallelFLOPS) + " MFLOPS"));
				}

				if (testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tComputations are equal: " + sequentialResult(1).equals(parallelResult(1))));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSpeedup: " + Formatting.getFormattedNumber(speedup)));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tEfficiency: " + efficiency));
				}

				delete matA;
				delete matB;
				if (testMode == 1 || testMode == 3) {
					delete sequentialResult(1);
				}
				if (testMode == 2 || testMode == 3) {
					delete parallelResult(1);
				}
			}
		} else {
			for run in 1..numRuns {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tRun " + run + " of " + numRuns + ":"));
				logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tSetting up matrices..."));
				var matA: Matrix(real) = new Matrix(real, dimA by 1);
				matA.insertRandomValues();
				var matB: Matrix(real) = new Matrix(real, dimB by 1);
				matB.insertRandomValues();
				nc_addstrln(" done");

				var sequentialResult: (Matrix(real), real);
				var parallelResult: (Matrix(real), real);

				if (testMode == 1 || testMode == 3) {
					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin sequential calculation..."));
					sequentialResult = sequentialMultiply(matA, matB);
					nc_addstrln(" done");
					sumSequentialTimes += sequentialResult(2);
					sequentialFLOPS = calcMegaFLOPS(matA, matB, sequentialResult(2));
					sumSequentialFLOPS += sequentialFLOPS;
				}

				if (testMode == 2 || testMode == 3) {
					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin parallel calculation..."));
					parallelResult = parallelMultiply(matA, matB);
					nc_addstrln(" done");
					sumParallelTimes += parallelResult(2);
					parallelFLOPS = calcMegaFLOPS(matA, matB, parallelResult(2));
					sumParallelFLOPS += parallelFLOPS;
				}

				if (testMode == 3) {
					speedup = Statistics.speedup(sequentialResult(2), parallelResult(2));
					efficiency = Statistics.efficiency(sequentialResult(2), parallelResult(2), HardwareInfo.getNumCores());
					sumSpeedups += speedup;
					sumEfficiencies += efficiency;
				}

				if (testMode == 1 || testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSequential time: " + Formatting.getFormattedNumber(sequentialResult(2)) + " seconds"));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSequential FLOPS: " + Formatting.getFormattedNumber(sequentialFLOPS) + " MFLOPS"));
				}
				if (testMode == 2 || testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel time: " + Formatting.getFormattedNumber(parallelResult(2)) + " seconds"));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel FLOPS: " + Formatting.getFormattedNumber(parallelFLOPS) + " MFLOPS"));
				}

				if (testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tComputations are equal: " + sequentialResult(1).equals(parallelResult(1))));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSpeedup: " + Formatting.getFormattedNumber(speedup)));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tEfficiency: " + efficiency));
				}

				delete matA;
				delete matB;
				if (testMode == 1 || testMode == 3) {
					delete sequentialResult(1);
				}
				if (testMode == 2 || testMode == 3) {
					delete parallelResult(1);
				}
			}

			if (testMode == 1 || testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage sequential times: " + Formatting.getFormattedNumber((sumSequentialTimes / numRuns)) + " seconds"));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage sequential FLOPS: " + Formatting.getFormattedNumber((sumSequentialFLOPS / numRuns)) + " MFLOPS"));
			}
			if (testMode == 2 || testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage parallel times: " + Formatting.getFormattedNumber((sumParallelTimes / numRuns)) + " seconds"));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage parallel FLOPS: " + Formatting.getFormattedNumber((sumParallelFLOPS / numRuns)) + " MFLOPS"));
			}
			if (testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage speedup: " + Formatting.getFormattedNumber((sumSpeedups / numRuns))));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage efficiency: " + (sumEfficiencies / numRuns)));
			}
		}

		var bufferedData = logger.close();
		return (fileName, bufferedData);
	}
}
