/*
	This module contains the mandelbrot set benchmark and its needed functions.
	The benchmark supports sequential and parallel computation and basic
	measuring.
*/
module MandelbrotBench {
	use Helpers;

	// General options
	var autoExit: bool = false;
	var logFile: bool = true;
	var testMode: int = 3; // possible values from 1..3, see enum modeText below
	var fileName: string = "ChapelBench_Mandelbrot_" +
						   Formatting.getFormattedDate("") +
						   "_" + Formatting.getFormattedTime("");
	var numRuns: uint = 3;
	enum boolText { no, yes }
	enum modeText { sequential, parallel, both }

	// Benchmark-specific options
	var imageDimension: int = 800;
	var offsetX: int = 0;
	var offsetY: int = 0;
	var zoom: int = 1;
	var iterations: int = 200;
	var imageColor: int = 1; // possible values from 1..8, see enum colorText below
	enum colorText { red, green, blue, cyan, magenta, yellow, gray, distinctive }

	/*
		Converts the values of the HSV color space to simple RGB values. Returns
		a tuple containing the values for red, green and blue.\\

		===== Parameters:
		* hue: real
			** represents the hue of the HSV color space
		* sat: real
			** represents the saturation of the HSV color space
		* val: real
			** represents the value of the HSV color space
		===== Returns:
		* (uint(8), uint(8), uint(8))
	*/
	proc hsvToRgb(hue: real, sat: real, val: real) {
		// http://www.cs.rit.edu/~ncs/color/t_convert.html#RGB%20to%20HSV%20&%20HSV%20to%20RGB
		var rgb: (uint(8), uint(8), uint(8));
		var i: int;
		var f, h, p, q, t: real;

		if (sat == 0) {
			return ((val*255):uint(8), (val*255):uint(8), (val*255):uint(8));
		}

		h = hue / 60;
		i = floor(h):int;
		f = h - i;
		p = val * (1 - sat);
		q = val * (1 - sat * f);
		t = val * (1 - sat * (1 - f));

		select (i) {
			when 0 {
				rgb = ((val*255):uint(8), (t*255):uint(8), (p*255):uint(8));
			}
			when 1 {
				rgb = ((q*255):uint(8), (val*255):uint(8), (p*255):uint(8));
			}
			when 2 {
				rgb = ((p*255):uint(8), (val*255):uint(8), (t*255):uint(8));
			}
			when 3 {
				rgb = ((p*255):uint(8), (q*255):uint(8), (val*255):uint(8));
			}
			when 4 {
				rgb = ((t*255):uint(8), (p*255):uint(8), (val*255):uint(8));
			}
			when 5 {
				rgb = ((val*255):uint(8), (p*255):uint(8), (q*255):uint(8));
			}
		}

		return rgb;
	}

	/*
		Chooses the colors for a pixel depending on the iterations the
		mandelbrot calculation and a color mode chosen by the user. Returns
		the respective red, green and blue values for the pixel.\\

		===== Parameters:
		* z: complex
			** the calculated mandelbrot value for the pixel
		* numIteration: int
			** the number of iterations needed for the calculation of z
		* maxIterations: int
			** the upper limit of the allowed iterations
		* colorMode: int
			** the user-specific color mode. Valid values are 1-8.
		===== Returns:
		* (uint(8), uint(8), uint(8))
	*/
	proc selectRGBValues(z: complex, numIteration: int, maxIterations: int, colorMode: int) {
		var zSqrt = sqrt(z.re * z.re + z.im * z.im);
		var brightness: int = (256 * log2(1.75 + numIteration - log2(log2(zSqrt))) / log2(maxIterations:real)):int;
		var rgb: (uint(8), uint(8), uint(8));
		select (colorMode) {
			when 1 { // red
				rgb = (255:uint(8), brightness:uint(8), brightness:uint(8));
			}
			when 2 { // green
				rgb = (brightness:uint(8), 255:uint(8), brightness:uint(8));
			}
			when 3 { // blue
				rgb = (brightness:uint(8), brightness:uint(8), 255:uint(8));
			}
			when 4 { // cyan
				rgb = (brightness:uint(8), 255:uint(8), 255:uint(8));
			}
			when 5 { // magenta
				rgb = (255:uint(8), brightness:uint(8), 255:uint(8));
			}
			when 6 { // yellow
				rgb = (255:uint(8), 255:uint(8), brightness:uint(8));
			}
			when 7 { // gray
				rgb = (brightness:uint(8), brightness:uint(8), brightness:uint(8));
			}
			when 8 { // distinctive
				rgb = hsvToRgb(numIteration * 6, 1, 1);
			}
		}

		return rgb;
	}

	/*
		Calculates a mandelbrot set and creates a graphical representation of
		it.\\

		===== Parameters:
		* fileName: string
			** the name of the image file
		* imageWidth: int
			** the width of the image file
		* imageHeight: int
			** the height of the image file
		* zoomFactor: int
			** determines how much to zoom into the mandelbrot set. Zoom point
			   is the origin of the complex plane. Defaults to 1.
		* xOffset: int
			** offset for the x value of the image origin. Defaults to 0.
		* yOffset: int
			** offset for the y value of the image origin. Defaults to 0.
		* maxIterations: int
			** the maximum allowed number of iterations per pixel. Defaults to
			   200.
		* colorMode: int
			** determines the background color of the mandelbrot (red, green,
			   blue, cyan, magenta, yellow, gray, distinctive). Distinctive
			   represents each iteration in a unique color, instead of having
			   a gradient coloring like in the other modes. Defaults to 1 (red).
		* start: int
			** the starting row of the mandelbrot set calculation; used for
			   parallel calculation. Defaults to 1.
		* end: int
			** the ending row of the mandelbrot set calculation; used for
			   parallel calculation. Defaults to the image height.
		* cMin: complex
			** the minimum values of the complex plane (real and imaginery)
		* cMax: complex
			** the maximum values of the complex plane (real and imaginery)
	*/
	proc mandelbrot(fileName: string, imageWidth: int, imageHeight: int, zoomFactor: int = 1, xOffset: int = 0, yOffset: int = 0, maxIterations: int = 200, colorMode: int = 1, start: int = 1, end: int = imageHeight, cMin: complex = -2.5-2.0i, cMax: complex = 1.5+2.0i) {
		// http://rosettacode.org/wiki/Mandelbrot_set#C
		// http://www.bananattack.com/stuff/FractalMaker/
		// http://stackoverflow.com/questions/16124127/improvement-to-my-mandelbrot-set-code
		var c: complex;
		var z: complex;
		var numIteration: int;
		var pixelWidth: real = (cMax.re - cMin.re) / imageWidth;
		var pixelHeight: real = (cMax.im - cMin.im)  / imageHeight;
		var color: (uint(8), uint(8), uint(8));
		var outputFile = open(fileName, iomode.cwr, style = new iostyle(max_width_bytes=max(uint(8))));
		var writer = outputFile.writer();
		if (start == 1 && end == imageHeight) {
			// the PPM header
			writer.write("P" + 3 + "\n" + imageWidth + "\n" + imageHeight + "\n" + 255 + "\n");
		}

		for y in start..end {
			c.im = (cMin.im + (y + yOffset) * pixelHeight) / zoomFactor;
			for x in 1..imageWidth {
				c.re = (cMin.re + (x + xOffset) * pixelWidth) / zoomFactor;
				z.re = 0.0;
				z.im = 0.0;
				numIteration = 0;
				while (numIteration < maxIterations && abs(z) <= 4) {
					if (abs(z) > 4) {
						break;
					}
					// the formula: z -> z^2 + c
					z = z * z + c;
					numIteration += 1;
				}
				if (numIteration == maxIterations) {
					// black
					color = (0:uint(8), 0:uint(8), 0:uint(8));
				} else {
					// colored
					color = selectRGBValues(z, numIteration, maxIterations, colorMode);
				}
				writer.writeln(color(1));
				writer.writeln(color(2));
				writer.writeln(color(3));
			}
		}
		writer.close();
		outputFile.fsync();
		outputFile.close();
	}

	/*
		Runs a sequential calculation of the mandelbrot set and measures the
		time it takes. Returns a tuple containing the image file name and the
		elapsed time.\\

		===== Parameters:
		* fileName: string
			** the name of the image file
		* imageWidth: int
			** the width of the image file
		* imageHeight: int
			** the height of the image file
		* zoomFactor: int
			** determines how much to zoom into the mandelbrot set. Zoom point
			   is the origin of the complex plane. Defaults to 1.
		* xOffset: int
			** offset for the x value of the image origin. Defaults to 0.
		* yOffset: int
			** offset for the y value of the image origin. Defaults to 0.
		* maxIterations: int
			** the maximum allowed number of iterations per pixel. Defaults to
			   200.
		* colorMode: int
			** determines the background color of the mandelbrot (red, green,
			   blue, cyan, magenta, yellow, gray, distinctive). Distinctive
			   represents each iteration in a unique color, instead of having
			   a gradient coloring like in the other modes. Defaults to 1 (red).
		* start: int
			** the starting row of the mandelbrot set calculation; used for
			   parallel calculation. Defaults to 1.
		* end: int
			** the ending row of the mandelbrot set calculation; used for
			   parallel calculation. Defaults to the image height.
		* cMin: complex
			** the minimum values of the complex plane (real and imaginery)
		* cMax: complex
			** the maximum values of the complex plane (real and imaginery)
	*/
	proc sequentialMandelbrot(fileName: string, imageWidth: int, imageHeight: int, zoomFactor: int = 1, xOffset: int = 0, yOffset: int = 0, maxIterations: int = 200, colorMode: int = 1, cMin: complex = -2.5-2.0i, cMax: complex = 1.5+2.0i) {
		var imageName: string = fileName + "_sequential.ppm";
		var t: Timer;

		t.start();
		mandelbrot(imageName, imageWidth, imageHeight, zoomFactor, xOffset, yOffset, maxIterations, colorMode, start = 1, end = imageHeight, cMin, cMax);
		t.stop();

		return (imageName, t.elapsed());
	}

	/*
		Runs a parallel calculation of the mandelbrot set and measures the
		time it takes. Parallelization is done by splitting the calculated rows
		of the images as equally as possible amongst the available cores. The
		resulting sub images are then being merged. Returns a tuple containing
		the image file name and the elapsed time.\\

		===== Parameters:
		* fileName: string
			** the name of the image file
		* imageWidth: int
			** the width of the image file
		* imageHeight: int
			** the height of the image file
		* zoomFactor: int
			** determines how much to zoom into the mandelbrot set. Zoom point
			   is the origin of the complex plane. Defaults to 1.
		* xOffset: int
			** offset for the x value of the image origin. Defaults to 0.
		* yOffset: int
			** offset for the y value of the image origin. Defaults to 0.
		* maxIterations: int
			** the maximum allowed number of iterations per pixel. Defaults to
			   200.
		* colorMode: int
			** determines the background color of the mandelbrot (red, green,
			   blue, cyan, magenta, yellow, gray, distinctive). Distinctive
			   represents each iteration in a unique color, instead of having
			   a gradient coloring like in the other modes. Defaults to 1 (red).
		* start: int
			** the starting row of the mandelbrot set calculation; used for
			   parallel calculation. Defaults to 1.
		* end: int
			** the ending row of the mandelbrot set calculation; used for
			   parallel calculation. Defaults to the image height.
		* cMin: complex
			** the minimum values of the complex plane (real and imaginery)
		* cMax: complex
			** the maximum values of the complex plane (real and imaginery)
	*/
	proc parallelMandelbrot(fileName: string, imageWidth: int, imageHeight: int, zoomFactor: int = 1, xOffset: int = 0, yOffset: int = 0, maxIterations: int = 200, colorMode: int = 1, cMin: complex = -2.5-2.0i, cMax: complex = 1.5+2.0i) {
		var imageName: string = fileName + "_parallel.ppm";
		var t: Timer;

		var outputFile = open(imageName, iomode.cw, style = new iostyle(max_width_bytes=max(uint(8))));
		var writer = outputFile.writer();
		// PPM header
		writer.write("P" + 3 + "\n" + imageWidth + "\n" + imageHeight + "\n" + 255 + "\n");

		t.start();
		var blockSize: int = ceil((imageHeight:real/HardwareInfo.getNumCores():real)):int;
		// distribute the calculation between the available cores
		forall i in 1..HardwareInfo.getNumCores() {
			var start: int = 1;
			if (i > 1) {
				start = (i - 1) * blockSize + 1;
			}
			var end: int = i * blockSize;
			if (end > imageHeight) {
				end = imageHeight;
			}
			mandelbrot("part" + i + ".tmp", imageWidth, imageHeight, zoomFactor, xOffset, yOffset, maxIterations, colorMode, start, end, cMin, cMax);
		}
		t.stop();

		// read the content from the file every thread creates and merge them
		for i in 1..HardwareInfo.getNumCores() {
			var err: syserr;
			var line: uint(8);
			var inputFile = open("part" + i + ".tmp", iomode.r, style = new iostyle(max_width_bytes=max(uint(8))));
			var reader = inputFile.reader();
			while(true) {
					reader.read(line, error=err);
					if (err == EEOF) {
						break;
					} else {
						writer.writeln(line);
					}
			}
			reader.close();
			inputFile.close();
			// delete the temporary files
			system("rm part" + i + ".tmp");
		}
		writer.close();
		outputFile.fsync();
		outputFile.close();

		return (imageName, t.elapsed());
	}

	/*
		Displays the main menu of the benchmark. Returns a boolean value to
		determine whether the application should be exited. True will be
		returned if autoExit is set or when the user requested to quit
		the program at the post test query.\\
	*/
	proc menu() {
		/*
			Refreshes the menu to display changes in the options.\\
		*/
		proc refresh() {
			nc_clear();
			nc_color_set(1);

			var menuList: list(string) = new list(string);

			menuList.append("Mandelbrot Set");
			menuList.append("");
			menuList.append("Calculates the mandelbrot set in the complex plane");
			menuList.append("from -2.5-2.0i to 1.5+2.0i and prints it into a file.");
			menuList.append("");
			menuList.append("\t[c] - Color: " + imageColor:colorText);
			menuList.append("\t[d] - Image dimension: " + Formatting.getFormattedNumber(imageDimension) + " x " + Formatting.getFormattedNumber(imageDimension));
			menuList.append("\t[i] - Max. iterations per pixel: " + Formatting.getFormattedNumber(iterations));
			menuList.append("\t[o] - Offset (pixel): (" + offsetX + "," + offsetY + ")");
			menuList.append("\t[z] - Zoom factor: " + Formatting.getFormattedNumber(zoom));
			menuList.append("");
			menuList.append("\t[q] - Return to main menu");
			menuList.append("\t[s] - Start benchmark");
			menuList.append("");
			menuList.append("Options:");
			menuList.append("\t[a] - Automatically quit program: " + (autoExit + 1):boolText);
			menuList.append("\t[l] - Create log file: " + (logFile + 1):boolText);
			menuList.append("\t[m] - Test mode: " + testMode:modeText);
			menuList.append("\t[n] - Log file name: " + fileName  + ".log");
			menuList.append("\t[r] - Number of test runs: " + Formatting.getFormattedNumber(numRuns));

			return menuList;
		}

		var menuList = refresh();
		var scroller = new ScrollArea(menuList);
		var input: int;

		label main while (input != 113) { // "q" is pressed
			nc_curs_set(0);
			nc_noecho();

			menuList = refresh();
			scroller.setData(menuList);
			scroller.printViewField();
			input = nc_getch();

			select (input) {
				when 97 { // "a" is pressed
					autoExit = !autoExit;
				}
				when 99 { // "c" is pressed
					imageColor = (imageColor % 8) + 1;
				}
				when 100 { // "d" is pressed
					imageDimension = Interface.setUnsignedValue("Enter image dimension (width = height):"):int;
					if (imageDimension < 1) {
						imageDimension = 800;
					}
				}
				when 105 { // "i" is pressed
					iterations = Interface.setUnsignedValue("Enter max. iterations per pixel:"):int;
					if (iterations < 1) {
						iterations = 200;
					}
				}
				when 108 { // "l" is pressed
					logFile = !logFile;
				}
				when 109 { // "m" is pressed
					testMode = (testMode % 3) + 1;
				}
				when 110 { // "n" is pressed
					fileName = Interface.setLogFileName("Mandelbrot");
				}
				when 111 { // "o" is pressed
					offsetX = Interface.setValue("Enter x offset:");
					offsetY = Interface.setValue("Enter y offset:");
				}
				when 114 { // "r" is pressed
					numRuns = Interface.setRuns();
				}
				when 115 { // "s" is pressed
					var result = run();

					if (autoExit) {
						result(2).destroy();
						return true;
					}
					if (Interface.postTestQuery(result(1), result(2))) {
						return true;
					}
				}
				when 122 { // "z" is pressed
					zoom = Interface.setValue("Enter zoom factor:");
					if (zoom < 1) {
						zoom = 1;
					}
				}
				when NC_KEY_UP {
					scroller.scrollUp();
				}
				when NC_KEY_DOWN {
					scroller.scrollDown();
				}
			}
		}
		return false;
	}

	/*
		Runs the mandelbrot set benchmark and returns a tuple containing
		the path/name of the log file and a list of strings containing the
		displayed text.\\

		===== Returns:
		* (string, list(string))
	*/
	proc run() {
		nc_clear();
		nc_curs_set(0);

		var logger: Logger;
		// if logging is disabled at least create a log file in the OS's temporary folder for debugging
		if (logFile) {
			logger = new Logger(fileName + ".log");
		} else {
			logger = new Logger();
		}

		logger.log("CPU " + getCPUID() + ":", true);
		logger.log("\tCores: " + getNumCores(), true);
		logger.log("\tAvailable Memory: " + getMemory(MemUnits.MB) + " MB", true);

		logger.log(nc_rmvaddstrln(0, 0, "Mandelbrot Set details:"));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tColor: " + imageColor:colorText));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tImage dimension: " + Formatting.getFormattedNumber(imageDimension) + " x " + Formatting.getFormattedNumber(imageDimension)));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tMax. iterations per pixel: " + Formatting.getFormattedNumber(iterations)));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tOffset (pixel): (" + offsetX + "," + offsetY + ")"));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tZoom factor: " + Formatting.getFormattedNumber(zoom)));


		var sequentialResult: (string, real);
		var parallelResult: (string, real);

		var speedup: real;
		var efficiency: real;

		// used for the calculation of the respective average
		var sumSequentialTimes: real;
		var sumParallelTimes: real;
		var sumSpeedups: real;
		var sumEfficiencies: real;


		for run in 1..numRuns {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tRun " + run + " of " + Formatting.getFormattedNumber(numRuns) + ":"));

			if (testMode == 1 || testMode == 3) {
				logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin sequential calculation..."));
				sequentialResult = sequentialMandelbrot(fileName, imageDimension, imageDimension, zoom, offsetX, offsetY, iterations, imageColor);
				nc_addstrln(" done");
				sumSequentialTimes += sequentialResult(2);
			}

			if (testMode == 2 || testMode == 3) {
				logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin parallel calculation..."));
				parallelResult = parallelMandelbrot(fileName, imageDimension, imageDimension, zoom, offsetX, offsetY, iterations, imageColor);
				nc_addstrln(" done");
				sumParallelTimes += parallelResult(2);
			}

			if (testMode == 3) {
				speedup = Statistics.speedup(sequentialResult(2), parallelResult(2));
				efficiency = Statistics.efficiency(sequentialResult(2), parallelResult(2), HardwareInfo.getNumCores());

				sumSpeedups += speedup;
				sumEfficiencies += efficiency;
			}

			if (testMode == 1 || testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSequential time: " + Formatting.getFormattedNumber(sequentialResult(2)) + " seconds"));
			}
			if (testMode == 2 || testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel time: " + Formatting.getFormattedNumber(parallelResult(2)) + " seconds"));
			}
			if (testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSpeedup: " + Formatting.getFormattedNumber(speedup)));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tEfficiency: " + efficiency));
			}
		}

		if (testMode == 1 || testMode == 3) {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tImage file name (sequential):"));
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\t" + sequentialResult(1)));
		}
		if (testMode == 2 || testMode == 3) {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tImage file name (parallel):"));
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\t" + parallelResult(1)));
		}
		if (testMode == 1 || testMode == 3) {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage sequential times: " + Formatting.getFormattedNumber((sumSequentialTimes / numRuns)) + " seconds"));
		}
		if (testMode == 2 || testMode == 3) {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage parallel times: " + Formatting.getFormattedNumber((sumParallelTimes / numRuns)) + " seconds"));
		}
		if (testMode == 3) {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage speedup: " + Formatting.getFormattedNumber((sumSpeedups / numRuns))));
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage efficiency: " + (sumEfficiencies / numRuns)));
		}

		var bufferedData = logger.close();
		return (fileName, bufferedData);
	}
}
