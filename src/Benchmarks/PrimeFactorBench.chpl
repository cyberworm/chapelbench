/*
	This module contains the prime factorization benchmark. The benchmark
	supports sequential and parallel computation and basic measuring. The
	algorithm used is a very basic one that can't be parallelized properly.
	The run time depends on the prime factors, more specifically on the
	distances between them. For example, the factorization of max(uint(64))
	takes not even a second with the factors
		3 × 5 × 17 × 257 × 641 × 65,537 × 6,700,417
	whereas the factorization of (max(uint(64)) / 4) takes about 40 minutes on
	a quad core computer with 3.4 GHz. The factors are
		3 × 715,827,883 × 2,147,483,647.
*/
module PrimeFactorBench {
	use Helpers;

	// General options
	var autoExit: bool = false;
	var logFile: bool = true;
	var testMode: int = 3; // possible values from 1..3, see enum modeText below
	var fileName: string = "ChapelBench_PrimeFactor_" +
						   Formatting.getFormattedDate("") +
						   "_" + Formatting.getFormattedTime("");
	var numRuns: uint = 3;
	enum boolText { no, yes }
	enum modeText { sequential, parallel, both }

	// Benchmark-specific options
	var numFactorizations: uint = 1;
	var value: uint = 70368744191279 / 16;

	/*
		Factorizes a given number using an optimized brute force method. Returns
		a list of the prime factors.\\

		===== Parameters:
		* value: uint
			** the number to be factorized
		===== Returns:
		* list(uint)
	*/
	proc factorize(value: uint) {
		var factorList: list(uint) = new list(uint);
		// don't do anything if the value is a prime
		if (ExtendedMath.isPrime(value)) {
			factorList.append(value);
			return factorList;
		}

		var residue: uint = value;
		var sqrtValue: uint = sqrt(value):uint;

		// find amount of prime 2 factors
		while(residue % 2 == 0) {
			factorList.append(2);
			residue /= 2;
		}

		// the actual factorization
		for i in 3..sqrtValue by 2 {
				if (ExtendedMath.isPrime(i)) {
					while(residue % i == 0) {
						factorList.append(i);
						residue /= i:uint;
					}
					if (ExtendedMath.isPrime(residue)) {
						break;
					}
				}
		}
		if (ExtendedMath.isPrime(residue)) {
			factorList.append(residue);
		}

		return factorList;
	}

	/*
		Runs a given amount of factorization in serial. Transforms the list
		of prime factors into an mathematically correct term. Returns a the
		formatted prime factorization and the elapsed time.\\

		===== Parameters:
		* value: uint
			** the number to be factorized
		* numFactorizations: uint
			** the number of factorization iterations
		===== Returns:
		* (list(string), real)
	*/
	proc sequentialFactorization(value: uint, numFactorizations: uint) {
		var primeFactorization: list(string) = new list(string);
		if (ExtendedMath.isPrime(value)) {
			primeFactorization.append(Formatting.getFormattedNumber(value));
			return (primeFactorization, 0.0);
		}
		var t: Timer;

		var factorizations: [1..numFactorizations] list(uint);

		t.start();
		for i in 1..numFactorizations {
			factorizations[i] = factorize(value);
		}
		t.stop();

		var lastPrime: uint;
		var count: uint;
		var remaining: int = factorizations[factorizations.numElements].length;

		for prime in factorizations[factorizations.numElements] {
			if (prime != lastPrime) {
				if (count > 0) {
					if (count < 2) {
						primeFactorization.append(Formatting.getFormattedNumber(lastPrime) + " x");
					} else {
						primeFactorization.append(Formatting.getFormattedNumber(lastPrime) + "^" + count + " x");
					}
				}
				lastPrime = prime;
				count = 1;
			} else {
				count += 1;
			}
			remaining -= 1;
			if (remaining == 0) {
				if (count < 2) {
					primeFactorization.append(Formatting.getFormattedNumber(prime));
				} else {
					primeFactorization.append(Formatting.getFormattedNumber(prime) + "^" + count);
				}
			}
		}

		for list in factorizations {
			list.destroy();
		}

		return (primeFactorization, t.elapsed());
	}

	/*
		Runs a given amount of factorization in parallel. Transforms the list
		of prime factors into an mathematically correct term. Returns a the
		formatted prime factorization and the elapsed time.\\

		===== Parameters:
		* value: uint
			** the number to be factorized
		* numFactorizations: uint
			** the number of factorization iterations
		===== Returns:
		* (list(string), real)
	*/
	proc parallelFactorization(value: uint, numFactorizations: uint) {
		var primeFactors: list(uint) = new list(uint);
		var primeFactorization: list(string) = new list(string);
		if (ExtendedMath.isPrime(value)) {
			primeFactorization.append(Formatting.getFormattedNumber(value));
			primeFactors.destroy();
			return (primeFactorization, 0.0);
		}

		var t: Timer;

		var factorizations: [1..numFactorizations] list(uint);

		t.start();
		forall i in 1..numFactorizations {
			factorizations[i] = factorize(value);
		}
		t.stop();


		var lastPrime: uint;
		var count: uint;
		var remaining: int = factorizations[factorizations.numElements].length;

		for prime in factorizations[factorizations.numElements] {
			if (prime != lastPrime) {
				if (count > 0) {
					if (count < 2) {
						primeFactorization.append(Formatting.getFormattedNumber(lastPrime) + " x");
					} else {
						primeFactorization.append(Formatting.getFormattedNumber(lastPrime) + "^" + count + " x");
					}
				}
				lastPrime = prime;
				count = 1;
			} else {
				count += 1;
			}
			remaining -= 1;
			if (remaining == 0) {
				if (count < 2) {
					primeFactorization.append(Formatting.getFormattedNumber(prime));
				} else {
					primeFactorization.append(Formatting.getFormattedNumber(prime) + "^" + count);
				}
			}
		}
		for list in factorizations {
			list.destroy();
		}

		return (primeFactorization, t.elapsed());
	}

	/*
		Displays the main menu of the benchmark. Returns a boolean value to
		determine whether the application should be exited. True will be
		returned if autoExit is set or when the user requested to quit
		the program at the post test query.\\
	*/
	proc menu() {
		/*
			Refreshes the menu to display changes in the options.\\
		*/
		proc refresh() {
			nc_clear();
			nc_color_set(1);

			var menuList: list(string) = new list(string);

			menuList.append("Prime Factorization");
			menuList.append("");
			menuList.append("Factorizes a natural number into a product of primes.");
			menuList.append("The run time depends on the found prime factors, more");
			menuList.append("specifically on the distances between them.");
			menuList.append("");
			menuList.append("\t[v] - Value to factorize: " + Formatting.getFormattedNumber(value));
			menuList.append("\t[f] - Amount of factorizations per mode: " + Formatting.getFormattedNumber(numFactorizations));
			menuList.append("");
			menuList.append("\t[q] - Return to main menu");
			menuList.append("\t[s] - Start benchmark");
			menuList.append("");
			menuList.append("Options:");
			menuList.append("");
			menuList.append("\t[a] - Automatically quit program: " + (autoExit + 1):boolText);
			menuList.append("\t[l] - Create log file: " + (logFile + 1):boolText);
			menuList.append("\t[m] - Test mode: " + testMode:modeText);
			menuList.append("\t[n] - Log file name: " + fileName + ".log");
			menuList.append("\t[r] - Number of test runs: " + Formatting.getFormattedNumber(numRuns));

			return menuList;
		}

		var menuList = refresh();
		var scroller = new ScrollArea(menuList);
		var input: int;

		label main while (input != 113) { // "q" is pressed
			nc_curs_set(0);
			nc_noecho();

			menuList = refresh();
			scroller.setData(menuList);
			scroller.printViewField();
			input = nc_getch();

			select (input) {
				when 97 { // "a" is pressed
					autoExit = !autoExit;
				}
				when 102 { // "f" is pressed
					numFactorizations = Interface.setUnsignedValue("Set number of factorizations:");
					if (numFactorizations == 0) {
						numFactorizations = 1;
					}
				}
				when 108 { // "l" is pressed
					logFile = !logFile;
				}
				when 109 { // "m" is pressed
					testMode = (testMode % 3) + 1;
				}
				when 110 { // "n" is pressed
					fileName = setLogFileName("PrimeFactor");
				}
				when 114 { // "r" is pressed
					numRuns = setRuns();
				}
				when 115 { // "s" is pressed
					var result = run();

					if (autoExit) {
						result(2).destroy();
						return true;
					}
					if (Interface.postTestQuery(result(1), result(2))) {
						return true;
					}
				}
				when 118 { // "v" is pressed
					value = Interface.setUnsignedValue("Enter number to factorize:");
					if (value == 0) {
						value = 70368744191279 / 16;
					}
				}
				when NC_KEY_UP {
					scroller.scrollUp();
				}
				when NC_KEY_DOWN {
					scroller.scrollDown();
				}
			}
		}
		return false;
	}

	/*
		Runs the prime factorization benchmark and returns a tuple containing
		the path/name of the log file and a list of strings containing the
		displayed text.\\

		===== Returns:
		* (string, list(string))
	*/
	proc run() {
		nc_clear();
		nc_curs_set(0);

		var logger: Logger;
		// if logging is disabled at least create a log file in the OS's temporary folder for debugging
		if (logFile) {
			logger = new Logger(fileName + ".log");
		} else {
			logger = new Logger();
		}

		logger.log("CPU " + getCPUID() + ":", true);
		logger.log("\tCores: " + getNumCores(), true);
		logger.log("\tAvailable Memory: " + getMemory(MemUnits.MB) + " MB", true);

		logger.log(nc_rmvaddstrln(0, 0, "Prime Factorization details:"));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tValue to factorize: " + Formatting.getFormattedNumber(value)));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAmount of factorizations per mode: " + Formatting.getFormattedNumber(numFactorizations)));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tTest mode: " + testMode:modeText));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tNumber of test runs: " + numRuns));

		var sequentialResult: (list(string), real);
		var parallelResult: (list(string), real);

		var speedup: real;
		var efficiency: real;

		// used for the calculation of the respective average
		var sumSequentialTimes: real;
		var sumParallelTimes: real;
		var sumSpeedups: real;
		var sumEfficiencies: real;

		for run in 1..numRuns {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tRun " + run + " of " + Formatting.getFormattedNumber(numRuns) + ":"));

			if (testMode == 1 || testMode == 3) {
				logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin sequential factorization (" + numFactorizations + " factorizations)..."));
				sequentialResult = sequentialFactorization(value, numFactorizations);
				nc_addstrln(" done");
				sumSequentialTimes += sequentialResult(2);
			}

			if (testMode == 2 || testMode == 3) {
				logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin parallel factorization (" + numFactorizations + " concurrent factorizations)..."));
				parallelResult = parallelFactorization(value, numFactorizations);
				nc_addstrln(" done");
				sumParallelTimes += parallelResult(2);
			}

			if (testMode == 3) {
				speedup = Statistics.speedup(sequentialResult(2), parallelResult(2));
				efficiency = Statistics.efficiency(sequentialResult(2), parallelResult(2), HardwareInfo.getNumCores());
				sumSpeedups += speedup;
				sumEfficiencies += efficiency;
			}

			if (testMode == 1 || testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSequential time: " + Formatting.getFormattedNumber(sequentialResult(2)) + " seconds"));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tPrime factors of " + Formatting.getFormattedNumber(value) + ": "));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\t\t" + sequentialResult(1):string));
			}
			if (testMode == 2 || testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel time: " + Formatting.getFormattedNumber(parallelResult(2)) + " seconds"));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tPrime factors of " + Formatting.getFormattedNumber(value) + ": "));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\t\t" + parallelResult(1):string));
			}
			if (testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSpeedup: " + Formatting.getFormattedNumber(speedup)));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tEfficiency: " + efficiency));
			}

			sequentialResult(1).destroy();
			parallelResult(1).destroy();
		}

		if (testMode == 1 || testMode == 3) {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage sequential times: " + Formatting.getFormattedNumber((sumSequentialTimes / numRuns)) + " seconds"));
		}
		if (testMode == 2 || testMode == 3) {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage parallel times: " + Formatting.getFormattedNumber((sumParallelTimes / numRuns)) + " seconds"));
		}
		if (testMode == 3) {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage speedup: " + Formatting.getFormattedNumber((sumSpeedups / numRuns))));
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage efficiency: " + (sumEfficiencies / numRuns)));
		}

		var bufferedData = logger.close();
		return (fileName, bufferedData);
	}
}
