/*
	This module contains the hyperoperation benchmark and its needed functions.
	The benchmark only supports sequential computation. Due to the nature of
	the hyperoperation, no user-specified value will be used.
*/
module HyperopBench {
	use Helpers;

	// General options
	var autoExit: bool = false;
	var logFile: bool = true;
	var fileName: string = "ChapelBench_Hyperop_" +
						   Formatting.getFormattedDate("") +
						   "_" + Formatting.getFormattedTime("");
	var numRuns: uint = 3;
	enum boolText { no, yes }

	/*
		The actual hyperoperation function. Depending on the input it does
		either an increment, an addition, a multiplication, an exponentiation
		or a tetration. Though higher sequences are possible in theory, the
		current limit is a tetration. Returns the result as integer.\\

		===== Parameters:
		* a: int
			** the first parameter of the hyperoperation a↑^(n-2)b
		* n: int
			** indicates the nature
			   of the hyperoperation: increment, addition, multiplication, etc.
	   * b: int
			** the second parameter of the hyperoperation a↑^(n-2)b
		===== Returns:
		* int
	*/
	proc hyper(a: int, n: int, b: int): int {
		// the return type is mandatory otherwise the compiler will complain
		// this check for negative numbers is needed since using uint will result in segfault
		if (a < 0 || n < 0 || b < 0) {
			return -1;
		}
		if (n == 0) {
			return b + 1;
		} else if (n == 1 && b == 0) {
			return a;
		} else if (n == 2 && b == 0) {
			return 0;
		} else if (n >= 3 && b == 0) {
			return 1;
		} else {
			return hyper(a, n-1, hyper(a, n, b - 1));
		}
	}

	/*
		Starts the hyperoperation and measures the elapsed time. Returns a tuple
		containing the result and the elapsed time.\\

		===== Parameters:
		* a: int
			** the first parameter of the hyperoperation a↑^(n-2)b
		* n: int
			** indicates the nature
			   of the hyperoperation: increment, addition, multiplication, etc.
	   * b: int
			** the second parameter of the hyperoperation a↑^(n-2)b
		===== Returns:
		* int
	*/
	proc sequentialHyperoperation(a: int, n: int, b: int) {
		var result: int;
		var t: Timer;

		t.start();
		result = hyper(a, n, b);
		t.stop();

		return (result, t.elapsed());
	}

	/*
		Displays the main menu of the benchmark. Returns a boolean value to
		determine whether the application should be exited. True will be
		returned if autoExit is set or when the user requested to quit
		the program at the post test query.\\
	*/
	proc menu() {
		/*
			Refreshes the menu to display changes in the options.\\
		*/
		proc refresh() {
			nc_clear();
			nc_color_set(1);

			var menuList: list(string) = new list(string);

			menuList.append("Hyperoperation");
			menuList.append("");
			menuList.append("Calculates the tetration (hyper-4) of 2 and 4 recursively.");
			menuList.append("That means 2 is exponentiated 4 times by itself:");
			menuList.append("\t2^2^2^2 (2^^4)");
			menuList.append("");
			menuList.append("\t[q] - Return to main menu");
			menuList.append("\t[s] - Start benchmark");
			menuList.append("");
			menuList.append("Options:");
			menuList.append("");
			menuList.append("\t[a] - Automatically quit program: " + (autoExit + 1):boolText);
			menuList.append("\t[l] - Create log file: " + (logFile + 1):boolText);
			menuList.append("\t[n] - Log file name: " + fileName + ".log");
			menuList.append("\t[r] - Number of test runs: " + Formatting.getFormattedNumber(numRuns));

			return menuList;
		}

		var menuList = refresh();
		var scroller = new ScrollArea(menuList);
		var input: int;

		label main while (input != 113) { // "q" is pressed
			nc_curs_set(0);
			nc_noecho();

			menuList = refresh();
			scroller.setData(menuList);
			scroller.printViewField();
			input = nc_getch();

			select (input) {
				when 97 { // "a" is pressed
					autoExit = !autoExit;
				}
				when 108 { // "l" is pressed
					logFile = !logFile;
				}
				when 110 { // "n" is pressed
					fileName = Interface.setLogFileName("Hyperop");
				}
				when 114 { // "r" is pressed
					numRuns = Interface.setRuns();
				}
				when 115 { // "s" is pressed
					var result = run();

					if (autoExit) {
						result(2).destroy();
						return true;
					}
					if (Interface.postTestQuery(result(1), result(2))) {
						return true;
					}
				}
				when NC_KEY_UP {
					scroller.scrollUp();
				}
				when NC_KEY_DOWN {
					scroller.scrollDown();
				}
			}
		}
		return false;
	}

	/*
		Runs the hyperoperation benchmark and returns a tuple containing
		the path/name of the log file and a list of strings containing the
		displayed text.\\

		===== Returns:
		* (string, list(string))
	*/
	proc run() {
		nc_clear();
		nc_curs_set(0);

		var logger: Logger;
		// if logging is disabled at least create a log file in the OS's temporary folder for debugging
		if (logFile) {
			logger = new Logger(fileName + ".log");
		} else {
			logger = new Logger();
		}

		logger.log("CPU " + getCPUID() + ":", true);
		logger.log("\tCores: " + getNumCores(), true);
		logger.log("\tAvailable Memory: " + getMemory(MemUnits.MB) + " MB", true);

		logger.log(nc_rmvaddstrln(0, 0, "Hyperoperation details:"));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tCalculated value: 2^2^2^2 (2^^4)"));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tNumber of test runs: " + numRuns));

		var sequentialResult: (int, real);

		// used for the calculation of the respective average
		var sumSequentialTimes: real;

		for run in 1..numRuns {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tRun " + run + " of " + Formatting.getFormattedNumber(numRuns) + ":"));

			logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin calculation..."));
			sequentialResult = sequentialHyperoperation(2, 4, 4);
			nc_addstrln(" done");
			sumSequentialTimes += sequentialResult(2);

			logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tResult: " + Formatting.getFormattedNumber(sequentialResult(1))));
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tElapsed time: " + Formatting.getFormattedNumber(sequentialResult(2)) + " seconds"));
		}

		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage sequential times: " + Formatting.getFormattedNumber((sumSequentialTimes / numRuns)) + " seconds"));

		var bufferedData = logger.close();
		return (fileName, bufferedData);
	}
}
