/*
	This module contains the prime sieving benchmark. The benchmark supports
	sequential and parallel computation and basic measuring. The algorithm
	used for the sieving is the 'Sieve of Eratosthenes'. This implemention
	is based on the C++ implemention by Stephan Brumme
	(http://create.stephan-brumme.com/eratosthenes/).
*/
module PrimeSieveBench {
	use Helpers;
	use Classes.CustomContainers;

	// General options
	var autoExit: bool = false;
	var logFile: bool = true;
	var distributed: bool = false;
	var testMode: int = 3; // possible values from 1..3, see enum testModeText below
	var fileName: string = "ChapelBench_PrimeNumberSieve_" +
						   Formatting.getFormattedDate("") +
						   "_" + Formatting.getFormattedTime("");
	var numRuns: uint = 3;
	enum boolText { no, yes }
	enum testModeText { sequential, parallel, both }

	// Benchmark-specific options
	var maxValue: uint = 1000;

	/*
		Does a blockwise prime number sieving as indicated by a starting and
		an end point.\\

		===== Parameters:
		* list: Vector(bool(8)) or DistVector(bool(8))
			** a reference to a list of bools representing the prime numbers
		* start: uint
			** the starting point of the sieving
		* end: uint
			** the end point of the sieving
	*/
	proc blockPrimeSieve(ref list, start: uint, end: uint) {
		for i in 3..sqrt(end):int by 2 {
			// skip multiples of the first odd numbers
			if (i >= 3*3 && i % 3 == 0) {
				continue;
			}
			if (i >= 5*5 && i % 5 == 0) {
				continue;
			}
			if (i >= 7*7 && i % 7 == 0) {
				continue;
			}
			if (i >= 9*9 && i % 9 == 0) {
				continue;
			}
			if (i >= 11*11 && i % 11 == 0) {
				continue;
			}
			if (i >= 13*13 && i % 13 == 0) {
				continue;
			}
			if (!list(i)) {
				// ignore numbers before the intervall
				var startValue: uint = ((start + i:uint) / i:uint) * i:uint;
				// make sure to start at the square of the current prime
				if (startValue < i:uint * i:uint) then startValue = i:uint * i:uint;
				// the starting number has to be odd
				if (startValue % 2 == 0) then startValue += i:uint;
				for j in startValue..end by i:uint * 2 {
				   list(j:int) = true;
				}
			}
		}
	}

	/*
		Prime sieving by using single primes only. Each function call only
		marks the multiples of a passed prime within a range, nothing more.\\

		===== Parameters:
		* list: Vector(bool(8)) or DistVector(bool(8))
			** a reference to a list of bools representing the prime numbers
		* prime: uint
			** the prime to sieve the multiples of
		* start: uint
			** the starting point of the sieving
		* end: uint
			** the end point of the sieving
	*/
	proc singlePrimeSieve(ref list, prime: uint, start: uint, end: uint) {
		// ignore numbers before the intervall
		var startValue: uint = ((start + prime) / prime) * prime;
		// make sure to start at the square of the current prime
		if (startValue < prime*prime) then startValue = prime*prime;
		// the starting number has to be odd
		if (startValue % 2 == 0) then startValue += prime;
		for j in startValue..end by prime * 2 {
			list(j:int) = true;
		}
	}

	/*
		Sets up the parallel blockwise sieving and distributes the list of
		primes as equally as possible amongst the available cores. Returns the
		number of found primes and the elapsed time.\\

		===== Parameters:
		* list: Vector(bool(8)) or DistVector(bool(8))
			** a reference to a list of bools representing the prime numbers
		* maxValue: uint
			** the length of the prime number list
		===== Returns:
		* (int, real)
	*/
	proc parallelSieveBlock(ref list, maxValue: uint) {
		// calculate the block size
		var blockSize: uint = ceil((maxValue:real/HardwareInfo.getNumCores():real)):uint;
		var t: Timer;

		// distribute each block amongst the available cores
		t.start();
		forall i in 1..HardwareInfo.getNumCores() {
			var start: uint = 3;
			if (i > 1) {
				start = (i:uint - 1) * blockSize;
			}
			var end: uint = i:uint * blockSize;
			if (end > maxValue) {
				end = maxValue;
			}
			blockPrimeSieve(list, start, end);
		}
		t.stop();

		// count all non-primes and increment the number of primes by one for prime '2'
		var nonPrimes: int = + reduce ([i in list.dim.dim(1)] list(i));
		var count: int = list.numElements - nonPrimes + 1;

		return (count, t.elapsed());
	}

	/*
		Sets up the parallel single prime sieving and distributes the list of
		primes as equally as possible amongst the available cores. Returns the
		number of found primes and the elapsed time.\\

		===== Parameters:
		* list: Vector(bool(8)) or DistVector(bool(8))
			** a reference to a list of bools representing the prime numbers
		* maxValue: uint
			** the length of the prime number list
		===== Returns:
		* (int, real)
	*/
	proc parallelSieveSingle(ref list, maxValue: uint) {
		// calculate the block size
		var blockSize: uint = ceil((maxValue:real/HardwareInfo.getNumCores():real)):uint;
		var t: Timer;

		// iterate over the list of primes and 'broadcast' the current prime to the threads
		t.start();
		for i in 3..sqrt(maxValue):int by 2 {
			if (!list(i)) {
				forall j in 1..HardwareInfo.getNumCores() {
					var start: uint = 3;
					if (j > 1) {
						start = (j:uint - 1) * blockSize;
					}
					var end: uint = j:uint * blockSize;
					if (end > maxValue) {
						end = maxValue;
					}
					singlePrimeSieve(list, i:uint, start, end);
				}
			}
		}
		t.stop();

		// count all non-primes and increment the number of primes by one for prime '2'
		var nonPrimes: int = + reduce ([i in list.dim.dim(1)] list(i));
		var count: int = list.numElements - nonPrimes + 1;

		return (count, t.elapsed());
	}

	/*
		Sets up the sequential sieving. Returns the number of
		found primes and the elapsed time.\\

		===== Parameters:
		* list: Vector(bool(8)) or DistVector(bool(8))
			** a reference to a list of bools representing the prime numbers
		* maxValue: uint
			** the length of the prime number list
		===== Returns:
		* (int, real)
	*/
	proc sequentialSieve(ref list, maxValue: uint) {
		var t: Timer;

		t.start();
		blockPrimeSieve(list, 2, maxValue);
		t.stop();

		// count all non-primes and increment the number of primes by one for prime '2'
		var nonPrimes: int = + reduce ([i in list.dim.dim(1)] list(i));
		var count: int = list.numElements - nonPrimes + 1;

		return (count, t.elapsed());
	}

	/*
		Displays the main menu of the benchmark. Returns a boolean value to
		determine whether the application should be exited. True will be
		returned if autoExit is set or when the user requested to quit
		the program at the post test query.\\
	*/
	proc menu() {
		/*
			Refreshes the menu to display changes in the options.\\
		*/
		proc refresh() {
			nc_clear();
			nc_color_set(1);

			var menuList: list(string) = new list(string);

			menuList.append("Prime Number Sieving");
			menuList.append("");
			menuList.append("Calculates the amount or primes up to a user specified");
			menuList.append("limit. Uses the 'Sieve of Erathosthenes' algorithm.");
			menuList.append("");
			menuList.append("\t[v] - Limit: " + Formatting.getFormattedNumber(maxValue));
			menuList.append("");
			menuList.append("\t[q] - Return to main menu");
			menuList.append("\t[s] - Start benchmark");
			menuList.append("");
			menuList.append("Options:");
			menuList.append("\t[a] - Automatically quit program: " + (autoExit+1):boolText);
			menuList.append("\t[d] - Use distributed memory: " + (distributed+1):boolText);
			menuList.append("\t[l] - Create log file: " + (logFile+1):boolText);
			menuList.append("\t[m] - Test mode: " + testMode:testModeText);
			menuList.append("\t[n] - Log file name: " + fileName + ".log");
			menuList.append("\t[r] - Number of test runs: " + Formatting.getFormattedNumber(numRuns));

			return menuList;
		}

		var menuList = refresh();
		var scroller = new ScrollArea(menuList);
		var input: int;

		label main while (input != 113) { // "q" is pressed
			nc_curs_set(0);
			nc_noecho();

			menuList = refresh();
			scroller.setData(menuList);
			scroller.printViewField();
			input = nc_getch();

			select (input) {
				when 97 { // "a" is pressed
					autoExit = !autoExit;
				}
				when 100 { // "d" is pressed
					if (HardwareInfo.getNumCPUs() > 1) {
						distributed = !distributed;
					} else {
						nc_color_set(2);
						nc_mvaddstr(NC_SCROLL_LINES+2, 0, "This option requires at least two CPUs!");
						nc_getch();
					}
				}
				when 108 { // "l" is pressed
					logFile = !logFile;
				}
				when 109 { // "m" is pressed
					testMode = (testMode % 3) + 1;
				}
				when 110 { // "n" is pressed
					fileName = Interface.setLogFileName("PrimeNumberSieve");
				}
				when 114 { // "r" is pressed
					numRuns = Interface.setRuns();
				}
				when 115 { // "s" is pressed
					var result: (string, list(string)) = run();

					if (autoExit) {
						result(2).destroy();
						return true;
					}
					if (Interface.postTestQuery(result(1), result(2))) {
						return true;
					}
				}
				when 118 { // "v" is pressed
					maxValue = Interface.setUnsignedValue("Enter max. value:");
					if (maxValue == 0) {
						maxValue = 1000;
					}
				}
				when NC_KEY_UP {
					scroller.scrollUp();
				}
				when NC_KEY_DOWN {
					scroller.scrollDown();
				}
			}
		}
		return false;
	}

	/*
		Runs the prime number sieving benchmark and returns a tuple containing
		the path/name of the log file and a list of strings containing the
		displayed text.\\

		===== Returns:
		* (string, list(string))
	*/
	proc run() {
		nc_clear();
		nc_curs_set(0);
		var logger: Logger;
		// if logging is disabled at least create a log file in the OS's temporary folder for debugging
		if (logFile) {
			logger = new Logger(fileName + ".log");
		} else {
			logger = new Logger();
		}

		logger.log("CPU " + getCPUID() + ":", true);
		logger.log("\tCores: " + getNumCores(), true);
		logger.log("\tAvailable Memory: " + getMemory(MemUnits.MB) + " MB", true);

		logger.log(nc_rmvaddstrln(0, 0, "Prime Number Sieving details:"));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tLimit: " + Formatting.getFormattedNumber(maxValue)));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tUsing distributed memory: " + (distributed+1):boolText));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tTest mode: " + testMode:testModeText));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tNumber of test runs: " + Formatting.getFormattedNumber(numRuns)));

		var list: Vector(bool(8));
		var listDistributed: DistVector(bool(8));

		var sequentialResult: (int, real);
		var parallelResultSingle: (int, real);
		var parallelResultBlock: (int, real);

		var equals: bool;
		var foundPrimes: int;
		var speedupSingle: real;
		var speedupBlock: real;
		var efficiencySingle: real;
		var efficiencyBlock: real;

		// used for the calculation of the respective average
		var sumSequentialTimes: real;
		var sumParallelTimesSingle: real;
		var sumParallelTimesBlock: real;
		var sumSpeedupsSingle: real;
		var sumSpeedupsBlock: real;
		var sumEfficienciesSingle: real;
		var sumEfficienciesBlock: real;


			for run in 1..numRuns {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tRun " + run + " of " + Formatting.getFormattedNumber(numRuns) + ":"));

				if (testMode == 1 || testMode == 3) {

					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tSetting up list..."));

					if (distributed) {
						listDistributed = new DistVector(bool(8), {3..maxValue:int by 2});
					} else {
						list = new Vector(bool(8), {3..maxValue:int by 2});
					}

					nc_addstrln(" done");

					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin sequential sieving..."));
					if (distributed) {
						sequentialResult = sequentialSieve(listDistributed, maxValue);
					} else {
						sequentialResult = sequentialSieve(list, maxValue);
					}
					nc_addstrln(" done");
					sumSequentialTimes += sequentialResult(2);
					foundPrimes = sequentialResult(1);

					if (distributed) {
						delete listDistributed ;
					} else {
						delete list;
					}
				}

				if (testMode == 2 || testMode == 3) {

					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tSetting up list..."));

					if (distributed) {
						listDistributed = new DistVector(bool(8), {3..maxValue:int by 2});
					} else {
						list = new Vector(bool(8), {3..maxValue:int by 2});
					}

					nc_addstrln(" done");

					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin parallel sieving (single prime)..."));
					if (distributed) {
						parallelResultSingle = parallelSieveSingle(listDistributed, maxValue);
					} else {
						parallelResultSingle = parallelSieveSingle(list, maxValue);
					}
					nc_addstrln(" done");
					sumParallelTimesSingle += parallelResultSingle(2);

					if (distributed) {
						delete listDistributed ;
					} else {
						delete list;
					}

					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tSetting up list..."));

					if (distributed) {
						listDistributed = new DistVector(bool(8), {3..maxValue:int by 2});
					} else {
						list = new Vector(bool(8), {3..maxValue:int by 2});
					}

					nc_addstrln(" done");

					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin parallel sieving (blockwise)..."));
					if (distributed) {
						parallelResultBlock = parallelSieveBlock(listDistributed, maxValue);
					} else {
						parallelResultBlock = parallelSieveBlock(list, maxValue);
					}
					nc_addstrln(" done");
					sumParallelTimesBlock += parallelResultBlock(2);
					foundPrimes = parallelResultBlock(1);

					equals = (parallelResultSingle(1) == parallelResultBlock(1));

					if (distributed) {
						delete listDistributed ;
					} else {
						delete list;
					}
				}

				if (testMode == 3) {
					speedupSingle = Statistics.speedup(sequentialResult(2), parallelResultSingle(2));
					speedupBlock = Statistics.speedup(sequentialResult(2), parallelResultBlock(2));
					efficiencySingle = Statistics.efficiency(sequentialResult(2), parallelResultSingle(2), HardwareInfo.getNumCores());
					efficiencyBlock = Statistics.efficiency(sequentialResult(2), parallelResultBlock(2), HardwareInfo.getNumCores());

					sumSpeedupsSingle += speedupSingle;
					sumSpeedupsBlock += speedupBlock;
					sumEfficienciesSingle += efficiencySingle;
					sumEfficienciesBlock += efficiencyBlock;

					equals = ((sequentialResult(1) == parallelResultSingle(1)) &&
							  (sequentialResult(1) == parallelResultBlock(1)) &&
							  (parallelResultSingle(1) == parallelResultBlock(1)));
				}

				logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tNumber of found primes: " + Formatting.getFormattedNumber(foundPrimes)));
				if (testMode == 1 || testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSequential time: " + Formatting.getFormattedNumber(sequentialResult(2)) + " seconds"));
				}
				if (testMode == 2 || testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel (single prime) time: " + Formatting.getFormattedNumber(parallelResultSingle(2)) + " seconds"));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel (blockwise) time: " + Formatting.getFormattedNumber(parallelResultBlock(2)) + " seconds"));
				}
				if (testMode == 2 || testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tNumber of found primes is equal: " + equals));
				}
				if (testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSpeedup (single prime): " + Formatting.getFormattedNumber(speedupSingle)));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSpeedup (blockwise): " + Formatting.getFormattedNumber(speedupBlock)));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tEfficiency (single prime): " + efficiencySingle));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tEfficiency (blockwise): " + efficiencyBlock));
				}
			}

		if (testMode == 1 || testMode == 3) {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage sequential times: " + Formatting.getFormattedNumber((sumSequentialTimes / numRuns)) + " seconds"));
		}
		if (testMode == 2 || testMode == 3) {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage parallel (single prime) times: " + Formatting.getFormattedNumber((sumParallelTimesSingle / numRuns)) + " seconds"));
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage parallel (blockwise) times: " + Formatting.getFormattedNumber((sumParallelTimesBlock / numRuns)) + " seconds"));
		}
		if (testMode == 3) {
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage speedup (single prime): " + Formatting.getFormattedNumber((sumSpeedupsSingle / numRuns))));
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage speedup (blockwise): " + Formatting.getFormattedNumber((sumSpeedupsBlock / numRuns))));
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage efficiency (single prime): " + (sumEfficienciesSingle / numRuns)));
			logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage efficiency (blockwise): " + (sumEfficienciesBlock / numRuns)));
		}

		var bufferedData = logger.close();
		return (fileName, bufferedData);
	}
}
