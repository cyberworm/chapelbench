/*
	This module contains the matrix-vector multiplication benchmark and
	its needed functions. The benchmark supports sequential and parallel
	computation and basic measuring. For now only one square matrix and one
	column vector will be used.
*/
module VectorBench {
	use Helpers;
	use Classes.CustomContainers;

	// General options
	var autoExit: bool = false;
	var distributed: bool = false;
	var logFile: bool = true;
	var testMode: int = 3; // possible values from 1..3, see enum modeText below
	var fileName: string = "ChapelBench_MatrixVectorMul_" +
						   Formatting.getFormattedDate("") +
						   "_" + Formatting.getFormattedTime("");
	var numRuns: uint = 3;
	enum boolText { no, yes }
	enum modeText { sequential, parallel, both }

	// Benchmark-specific options
	var dimension: uint = 640;

	/*
		Calculates and returns the floating point operation per seconds of a
		matrix-matrix multiplication.\\

		===== Parameters:
		* matrix
			** the matrix (Matrix or DistMatrix) used in the
			   multiplication
	   * time: real
			** a floating point number representing the elapsed time (in
			   seconds) of the multiplication
		===== Returns:
		* real
	*/
	proc calcMegaFLOPS(ref matrix, time: real) {
		// TODO: let the user choose the unit of the result: FLOPS, kFLOPS, etc
		var FLOPS: int = (2 * matrix.numColumns - 1) * matrix.numRows;
		var kFLOPS: real = FLOPS / 1000;
		var MFLOPS: real = kFLOPS / 1000;
		return MFLOPS / time;
	}

	/*
		Concatinates several vectors as provided in an array.\\

		===== Parameters:
		* vectors: [] Vector(real)
			** an array holding the vectors to be concatenated
		===== Returns:
		* Vector(real)
	*/
	proc concat(ref vectors: [] Vector(real)) {
		var totalLength: int = + reduce ([i in vectors] i.numElements);

		var newElements: [1..totalLength] real;

		var start: int = 1;
		var end: int;

		// iterate over the array, extract the elements from each vector and
		// insert them into the array for the new vector
		for vector in vectors {
			end = vector.numElements + start - 1;
			newElements[start..end] = vector.elements;
			start = end+1;
		}

		var newVec: Vector(real) = new Vector(real, totalLength);
		for i in newElements.domain {
			newVec(i) = newElements[i];
		}

		return newVec;
	}

	/*
		Concatinates several vectors as provided in an array.\\

		===== Parameters:
		* vectors: [] DistVector(real)
			** an array holding the vectors to be concatenated
		===== Returns:
		* DistVector(real)
	*/
	proc concatDistributed(ref vectors: [] DistVector(real)) {
		var totalLength: int = + reduce ([i in vectors] i.numElements);

		var newElements: [1..totalLength] real;

		var start: int = 1;
		var end: int;

		// iterate over the array, extract the elements from each vector and
		// insert them into the array for the new vector
		for vector in vectors {
			end = vector.numElements + start - 1;
			newElements[start..end] = vector.elements;
			start = end+1;
		}

		var newVec: DistVector(real) = new DistVector(real, totalLength);
		for i in newElements.domain {
			newVec[i] = newElements[i];
		}

		return newVec;
	}

	/*
		Returns the product of a matrix and a vector which has been calculated
		in parallel and the elapsed time.\\

		===== Parameters:
		* A: Matrix(real)
			** the first factor of the product
		* b: Vector(real)
			** the second factor of the product
		===== Returns:
		* (Vector(real), real)
	*/
	proc parallelMultiply(ref A: Matrix(real), ref b: Vector(real)) {
	/*
		Calculation is done by splitting one matrix as equally as possible into
		smaller submatrices along its rows and distribute them to a number of
		tasks (which corresponds with the number of available cores), so that
		each thread calculates a fragment of the result in serial. After each
		thread is finished the resulting submatrices are being concatenated to
		form the resulting matrix.
	*/
		var cores: int = HardwareInfo.getNumCores();
		//  task syncronization variables
		var count$: sync int = cores;
		var release$: single bool;

		var t: Timer;

		var input: [1..cores] Matrix(real);
		var output: [1..cores] Vector(real);

		var step: int = A.numRows / cores;
		var start: int = 1;
		var end: int;

		// split matrix A into several submatrices
		for i in 1..cores {
			end = step * i;
			if (i == cores) then end = A.numRows;
			input[i] = A.submatrix(start..end);
			start = end + 1;
		}

		// begin of the calculation in parallel
		t.start();
		forall i in 1..cores {
			/*
				Instead of using a forall-loop and the syncronization variables
				the 'sync' keyword could have been used in a normal for-loop,
				but this method proofed to be more efficient.
			*/
			output[i] = sequentialMultiply(input[i], b)(1);

			// set the state of count$ to empty to block other tasks
			var remainingTasks: int = count$;

			if (remainingTasks != 1) {
				// set the state of count$ to full so other tasks can proceed
				count$ = remainingTasks-1;
				// all but the last task will wait here until release$ is assigned
				release$;
			} else {
				// the last task will reach this point and assign the variable
				release$ = true;
			}
		}
		t.stop();

		// concat the resulting submatrices
		var newVec: Vector(real) = concat(output);

		// delete the initial and resulting submatrices to free memory
		for (i, j) in zip (input, output) {
			delete i;
			delete j;
		}

		return (newVec, t.elapsed());
	}

	/*
		Returns the product of a matrix and a vector which has been calculated
		in parallel and the elapsed time; using distributed memory.\\

		===== Parameters:
		* A: DistMatrix(real)
			** the first factor of the product
		* b: DistVector(real)
			** the second factor of the product
		===== Returns:
		* (DistVector(real), real)
	*/
	proc parallelMultiplyDistributed(ref A: DistMatrix(real), ref b: DistVector(real)) {
	/*
		Calculation is done by splitting one matrix as equally as possible into
		smaller submatrices along its rows and distribute them to a number of
		tasks (which corresponds with the number of available cores), so that
		each thread calculates a fragment of the result in serial. After each
		thread is finished the resulting submatrices are being concatenated to
		form the resulting matrix.
	*/
		var cores: int = HardwareInfo.getNumCores();
		//  task syncronization variables
		var count$: sync int = cores;
		var release$: single bool;

		var t: Timer;

		var input: [1..cores] DistMatrix(real);
		var output: [1..cores] DistVector(real);

		var step: int = A.numRows / cores;
		var start: int = 1;
		var end: int;

		// split matrix A into several submatrices
		for i in 1..cores {
			end = step * i;
			if (i == cores) then end = A.numRows;
			input[i] = A.submatrix(start..end);
			start = end+1;
		}

		// begin of the calculation in parallel
		t.start();
		forall i in 1..cores {
			output[i] = sequentialMultiplyDistributed(input[i], b)(1);

			// set the state of count$ to empty to block other tasks
			var remainingTasks: int = count$;

			if (remainingTasks != 1) {
				// set the state of count$ to full so other tasks can proceed
				count$ = remainingTasks-1;
				// all but the last task will wait here until release$ is assigned
				release$;
			} else {
				// the last task will reach this point and assign the variable
				release$ = true;
			}
		}
		t.stop();

		// concat the resulting submatrices
		var newVec: DistVector(real) = concatDistributed(output);

		// delete the initial and resulting submatrices to free memory
		for (i, j) in zip (input, output) {
			delete i;
			delete j;
		}

		return (newVec, t.elapsed());
	}

	/*
		Returns the product of a matrix and a vector and the elapsed time.\\

		===== Parameters:
		* A: Matrix(real)
			** the first factor of the product
		* b: Vector(real)
			** the second factor of the product
		===== Returns:
		* (Vector(real), real)
	*/
	proc sequentialMultiply(ref A: Matrix(real), ref b: Vector(real)) {
		assert(A.numColumns == b.numElements, "Number of columns of matrix A has to match length of vector b");
		var result: Vector(real) = new Vector(real, A.numRows);

		var value: real;
		var t: Timer;

		t.start();
		for row in 1..A.numRows {
				value = 0;
				for (rowElement, positionElement) in zip (A.row(row), b.elements) {
					value += rowElement * positionElement;
				}
				result(row) = value;
		}
		t.stop();

		return (result, t.elapsed());
	}

	/*
		Returns the product of a matrix and a vector and the elapsed time; using
		distributed memory.\\

		===== Parameters:
		* A: DistMatrix(real)
			** the first factor of the product
		* b: DistVector(real)
			** the second factor of the product
		===== Returns:
		* (DistVector(real), real)
	*/
	proc sequentialMultiplyDistributed(ref A: DistMatrix(real), ref b: DistVector(real)) {
		assert(A.numColumns == b.numElements, "Number of columns of matrix A has to match length of vector b");
		var result: DistVector(real) = new DistVector(real, A.numRows);

		var value: real;
		var t: Timer;

		t.start();
		for row in 1..A.numRows {
				value = 0;
				for (rowElement, positionElement) in zip (A.row(row), b.elements) {
					value += rowElement*positionElement;
				}
				result(row) = value;
		}
		t.stop();

		return (result, t.elapsed());
	}

	/*
		Displays the main menu of the benchmark. Returns a boolean value to
		determine whether the application should be exited. True will be
		returned if autoExit is set or when the user requested to quit
		the program at the post test query.\\
	*/
	proc menu() {
		/*
			Refreshes the menu to display changes in the options.\\
		*/
		proc refresh() {
			nc_clear();
			nc_color_set(1);

			var menuList: list(string) = new list(string);

			menuList.append("Matrix-Vector Multiplication");
			menuList.append("");
			menuList.append("Calculates the product of a matrix and a vector.");
			menuList.append("Calculation time and memory usage grows exponentially");
			menuList.append("with the dimension of the matrix.");
			menuList.append("");
			menuList.append("\t[v] - Matrix / Vector size: " + Formatting.getFormattedNumber(dimension));
			menuList.append("");
			menuList.append("\t[q] - Return to main menu");
			menuList.append("\t[s] - Start benchmark");
			menuList.append("");
			menuList.append("Options:");
			menuList.append("");
			menuList.append("\t[a] - Automatically quit program: " + (autoExit + 1):boolText);
			menuList.append("\t[d] - Use distributed memory: " + (distributed + 1):boolText);
			menuList.append("\t[l] - Create log file: " + (logFile + 1):boolText);
			menuList.append("\t[m] - Test mode: " + testMode:modeText);
			menuList.append("\t[n] - Log file name: " + fileName + ".log");
			menuList.append("\t[r] - Number of test runs: " + Formatting.getFormattedNumber(numRuns));

			return menuList;
		}

		var menuList = refresh();
		var scroller = new ScrollArea(menuList);
		var input: int;

		label main while (input != 113) { // "q" is pressed
			nc_curs_set(0);
			nc_noecho();

			menuList = refresh();
			scroller.setData(menuList);
			scroller.printViewField();
			input = nc_getch();

			select (input) {
				when 97 { // "a" is pressed
					autoExit = !autoExit;
				}
				when 100 { // "d" is pressed
					if (HardwareInfo.getNumCPUs() > 1) {
						distributed = !distributed;
					} else {
						nc_color_set(2);
						nc_mvaddstr(NC_SCROLL_LINES+2, 0, "This option requires at least two CPUs!");
						nc_getch();
					}
				}
				when 108 { // "l" is pressed
					logFile = !logFile;
				}
				when 109 { // "m" is pressed
					testMode = (testMode % 3) + 1;
				}
				when 110 { // "n" is pressed
					fileName = Interface.setLogFileName("MatrixVectorMul");
				}
				when 114 { // "r" is pressed
					numRuns = Interface.setRuns();
				}
				when 115 { // "s" is pressed
					var result: (string, list(string)) = VectorBench.run({1..dimension:int, 1..dimension:int}, {1..dimension:int});

					if (autoExit) {
						result(2).destroy();
						return true;
					}
					if (Interface.postTestQuery(result(1), result(2))) {
						return true;
					}
				}
				when 118 { // "v" is pressed
					dimension = Interface.setUnsignedValue("Enter dimension (rows = columns = length):");
					if (dimension == 0) {
						dimension = 640;
					}
				}
				when NC_KEY_UP {
					scroller.scrollUp();
				}
				when NC_KEY_DOWN {
					scroller.scrollDown();
				}
			}
		}
		return false;
	}

	/*
		Runs the matrix-vector multiplication benchmark and returns a
		tuple containing the path/name of the log file and a list of strings
		containing the displayed text.\\

		===== Parameters:
		* dimA: domain(2)
			** the dimension of the matrix
		* dimB: domain(2)
			** the dimension of the vector
		===== Returns:
		* (string, list(string))
	*/
	proc run(dimA: domain(2), dimB: domain(1)) {
		nc_clear();
		nc_curs_set(0);
		var logger: Logger;
		// if logging is disabled at least create a log file in the OS's temporary folder for debugging
		if (logFile) {
			logger = new Logger(fileName + ".log");
		} else {
			logger = new Logger();
		}

		logger.log("CPU " + getCPUID() + ":", buffered = false);
		logger.log("\tCores: " + getNumCores(), buffered = false);
		logger.log("\tAvailable Memory: " + getMemory(MemUnits.MB) + " MB", buffered = false);

		logger.log(nc_rmvaddstrln(0, 0, "Matrix-Vector Multiplication details:"));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tMatrix A dimension: " + Formatting.getFormattedNumber(dimA.high[1]) + "x" + Formatting.getFormattedNumber(dimA.high[2])));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tVector b length: " + Formatting.getFormattedNumber(dimB.high)));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tUsing distributed memory: " + (distributed + 1):boolText));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tTest mode: " + testMode:modeText));
		logger.log(nc_rmvaddstrln(NC_Y, 0, "\tNumber of test runs: " + Formatting.getFormattedNumber(numRuns)));

		var sequentialFLOPS: real;
		var parallelFLOPS: real;
		var speedup: real;
		var efficiency: real;

		// used for the calculation of the respective average
		var sumSequentialTimes: real;
		var sumParallelTimes: real;
		var sumSequentialFLOPS: real;
		var sumParallelFLOPS: real;
		var sumSpeedups: real;
		var sumEfficiencies: real;

		if (distributed) {
			for run in 1..numRuns {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tRun " + run + " of " + Formatting.getFormattedNumber(numRuns) + ":"));
				logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tSetting up matrix and vector..."));
				var matrix: DistMatrix(real) = new DistMatrix(real, dimA by 1);
				matrix.insertRandomValues();
				var vector: DistVector(real) = new DistVector(real, dimB.high);
				vector.insertRandomValues();
				nc_addstrln(" done");

				var sequentialResult: (DistVector(real), real);
				var parallelResult: (DistVector(real), real);

				if (testMode == 1 || testMode == 3) {
					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin sequential multiplication..."));
					sequentialResult = sequentialMultiplyDistributed(matrix, vector);
					nc_addstrln(" done");
					sumSequentialTimes += sequentialResult(2);
					sequentialFLOPS = calcMegaFLOPS(matrix, sequentialResult(2));
					sumSequentialFLOPS += sequentialFLOPS;
				}

				if (testMode == 2 || testMode == 3) {
					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin parallel multiplication..."));
					parallelResult = parallelMultiplyDistributed(matrix, vector);
					nc_addstrln(" done");
					sumParallelTimes += parallelResult(2);
					parallelFLOPS = calcMegaFLOPS(matrix, parallelResult(2));
					sumParallelFLOPS += parallelFLOPS;
				}

				if (testMode == 3) {
					speedup = Statistics.speedup(sequentialResult(2), parallelResult(2));
					efficiency = Statistics.efficiency(sequentialResult(2), parallelResult(2), HardwareInfo.getNumCores());
					sumSpeedups += speedup;
					sumEfficiencies += efficiency;
				}

				if (testMode == 1 || testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSequential time: " + Formatting.getFormattedNumber(sequentialResult(2)) + " seconds"));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSequential FLOPS: " + Formatting.getFormattedNumber(sequentialFLOPS) + " MFLOPS"));
				}
				if (testMode == 2 || testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel time: " + Formatting.getFormattedNumber(parallelResult(2)) + " seconds"));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel FLOPS: " + Formatting.getFormattedNumber(parallelFLOPS) + " MFLOPS"));
				}

				if (testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tComputations are equal: " + sequentialResult(1).equals(parallelResult(1))));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSpeedup: " + Formatting.getFormattedNumber(speedup)));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tEfficiency: " + efficiency));
				}

				delete matrix;
				delete vector;
				if (testMode == 1 || testMode == 3) {
					delete sequentialResult(1);
				}
				if (testMode == 2 || testMode == 3) {
					delete parallelResult(1);
				}
			}
		} else {
			for run in 1..numRuns {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tRun " + run + " of " + numRuns + ":"));
				logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tSetting up matrix and vector..."));
				var matrix: Matrix(real) = new Matrix(real, dimA by 1);
				matrix.insertRandomValues();
				var vector: Vector(real) = new Vector(real, dimB.high);
				vector.insertRandomValues();
				nc_addstrln(" done");

				var sequentialResult: (Vector(real), real);
				var parallelResult: (Vector(real), real);

				if (testMode == 1 || testMode == 3) {
					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin sequential calculation..."));
					sequentialResult = sequentialMultiply(matrix, vector);
					nc_addstrln(" done");
					sumSequentialTimes += sequentialResult(2);
					sequentialFLOPS = calcMegaFLOPS(matrix, sequentialResult(2));
					sumSequentialFLOPS += sequentialFLOPS;
				}

				if (testMode == 2 || testMode == 3) {
					logger.log(nc_rmvaddstr(NC_Y, 0, "\t\tBegin parallel calculation..."));
					parallelResult = parallelMultiply(matrix, vector);
					nc_addstrln(" done");
					sumParallelTimes += parallelResult(2);
					parallelFLOPS = calcMegaFLOPS(matrix, parallelResult(2));
					sumParallelFLOPS += parallelFLOPS;
				}

				if (testMode == 3) {
					speedup = Statistics.speedup(sequentialResult(2), parallelResult(2));
					efficiency = Statistics.efficiency(sequentialResult(2), parallelResult(2), HardwareInfo.getNumCores());
					sumSpeedups += speedup;
					sumEfficiencies += efficiency;
				}

				if (testMode == 1 || testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSequential time: " + Formatting.getFormattedNumber(sequentialResult(2)) + " seconds"));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSequential FLOPS: " + Formatting.getFormattedNumber(sequentialFLOPS) + " MFLOPS"));
				}
				if (testMode == 2 || testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel time: " + Formatting.getFormattedNumber(parallelResult(2)) + " seconds"));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tParallel FLOPS: " + Formatting.getFormattedNumber(parallelFLOPS) + " MFLOPS"));
				}

				if (testMode == 3) {
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tComputations are equal: " + sequentialResult(1).equals(parallelResult(1))));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tSpeedup: " + Formatting.getFormattedNumber(speedup)));
					logger.log(nc_rmvaddstrln(NC_Y, 0, "\t\tEfficiency: " + efficiency));
				}

				delete matrix;
				delete vector;
				if (testMode == 1 || testMode == 3) {
					delete sequentialResult(1);
				}
				if (testMode == 2 || testMode == 3) {
					delete parallelResult(1);
				}
			}

			if (testMode == 1 || testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage sequential times: " + Formatting.getFormattedNumber((sumSequentialTimes / numRuns)) + " seconds"));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage sequential FLOPS: " + Formatting.getFormattedNumber((sumSequentialFLOPS / numRuns)) + " MFLOPS"));
			}
			if (testMode == 2 || testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage parallel times: " + Formatting.getFormattedNumber((sumParallelTimes / numRuns)) + " seconds"));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage parallel FLOPS: " + Formatting.getFormattedNumber((sumParallelFLOPS / numRuns)) + " MFLOPS"));
			}
			if (testMode == 3) {
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage speedup: " + Formatting.getFormattedNumber((sumSpeedups / numRuns))));
				logger.log(nc_rmvaddstrln(NC_Y, 0, "\tAverage efficiency: " + (sumEfficiencies / numRuns)));
			}
		}

		var bufferedData = logger.close();
		return (fileName, bufferedData);
	}
}
