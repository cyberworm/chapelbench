/*
	This module provides helper functions that are independent of any specific
	benchmark. This includes an expansion of the Timer module for formatted time
	and date, some 'advanced' printing functions, basic system hardware
	information and a rudimentary logging framework.
*/
module Helpers {
	use ExtendedMath;
	use Formatting;
	use HardwareInfo;
	use Interface;
	use Logging;
	use Statistics;
}
