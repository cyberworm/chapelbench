ChapelBench v0.8
================
ChapelBench is a CPU benchmarking program focusing on parallel computation.
It is written in the parallel programming language Chapel.

Requirements
------------
- ncurses >= 5.9

COMPILING
============
Requirements
------------
- Chapel >= 1.7
- libncurses5-dev >= 5.9
- libtinfo-dev >= 5.9

**Note:** the release version of Chapel 1.7 has some missing reserved symbols.
	Please use the development version from the official svn repository:

	svn checkout svn://svn.code.sf.net/p/chapel/code/trunk chapel-code

documentation:

- Python 2.7
- python-creoleparser >= 0.7.4

Commands
--------
- git clone git@bitbucket.org:cyberworm/chapelbench.git
- $ cd chapelbench/src
- $ make

See ./src/Makefile for further options

VERSION HISTORY
===============
v0.8 - 2013-07-15
-----------------
- new benchmark: Mandelbrot set
- bugfixes and code optimizations

v0.6 - 2013-07-05
-----------------
- new benchmarks: Hyperoperation and Ackermann Function (sequential only)

v0.4 - 2013-07-03
-----------------
- new benchmark: Prime Factorization
- the user can now choose between testing modes: sequential only, parallel only
  and both
- the tab size has been reduced to 2 to preserver space in the terminal

v0.3 - 2013-06-21
-----------------
- dropped the non-ncurses version to reduce development time
- introducing a new benchmark: Prime Number Sieving
- changed the menu behaviour. Each test has its own sub menu and options
- the user can now specify a name for the log file

v0.2 - 2013-06-05
-----------------
- introducing a new benchmark: Matrix-Vector Multiplication
- various bugfixes

v0.1.5 - 2013-06-03
-------------------
- ncurses can now be used as interface, compile using 'make ncurses'. The
  Resulting binary is called 'ChapelBench_NC'
- the ncurses version offers additional usability by using colors
  (error = red, input = blue, note = green)
- the ncurses version might become the default version in the future, with
  the non-ncurses version being used for compatibility or stripped complete-
  ly. Depends on a possible Microsoft Windows version

v0.1 - 2013-05-27
-----------------
- initial version featuring matrix-matrix multiplication benchmark
